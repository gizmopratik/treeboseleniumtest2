package base;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

public class DriverManager {

	private DriverManager() {
		// Do nothing
	}

	private static DriverManager driverManager = new DriverManager();

	public static DriverManager getInstance() {
		return driverManager;
	}

	// To use as e.g.
	// WebDriver driver = DriverManager.getInstance().getDriver();
	// driver.getTitle();
	private static ThreadLocal<RemoteWebDriver> driver = new ThreadLocal<RemoteWebDriver>() {
		@Override
		protected RemoteWebDriver initialValue() {
			System.setProperty("webdriver.chrome.driver", "/usr/local/share/chromedriver");
			String browser = System.getProperty("browser");

			if (browser == null || browser.isEmpty()) {
				browser = "chrome";
				System.out.println("No browser provided! defaulting to " + browser);
			} else {
				System.out.println("Browser : " + browser);
			}

			String host = System.getProperty("host");
			DesiredCapabilities cap = null;
			if (browser.equals("firefox")) {
				cap = DesiredCapabilities.firefox();
				cap.setBrowserName("firefox");
			} else if (browser.equals("chrome")) {
				cap = DesiredCapabilities.chrome();
				cap.setBrowserName("chrome");
			} else if (browser.equals("chromeMobile")) {
				Map<String, String> mobileEmulation = new HashMap<String, String>();
				mobileEmulation.put("deviceName", "Google Nexus 5");

				Map<String, Object> chromeOptions = new HashMap<String, Object>();
				chromeOptions.put("mobileEmulation", mobileEmulation);
				cap = DesiredCapabilities.chrome();
				cap.setCapability(ChromeOptions.CAPABILITY, chromeOptions);
			}

			URL url = null;
			try {
				url = new URL("http://172.40.20.159:4444/wd/hub");
			} catch (MalformedURLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			if (host == null || host.isEmpty()) {
				host = "grid";
			}

			if (host.equals("grid")) {
				return new RemoteWebDriver(url, cap);
			} else if (host.equals("localhost")) {
				if (browser.equals("firefox")) {
					return new FirefoxDriver(cap);
				} else if (browser.equals("chrome")) {
					return new ChromeDriver(cap);
				} else if (browser.equals("chromeMobile")) {
					return new ChromeDriver(cap);
				}
			}
			return null;
		}
	};

	public WebDriver getDriver() {
		return driver.get();
	}

	public void removeDriver() {
		driver.get().quit();
		driver.remove();
	}
}