package base;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import org.w3c.dom.Node;
import org.w3c.dom.Element;

import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.concurrent.ConcurrentSkipListMap;

public class HotelLogixAPI {
	final String CONSUMER_KEY = "B39CEE50AC4B6925F9697409E2D84C88746593E5";
	final String CONSUMER_SECRET = "9110CC27311F94542E7F5ACF3643B3909029F570";
	final String HOTELOGIX_URL = "https://crs.staygrid.com/ws/web/";

	/**
	 * create signature
	 * @param keyString
	 * @param msg
	 * @return
	 */
 
	public static String hmacDigest(String keyString, String msg) {
		String digest = null;
		try {
			SecretKeySpec key = new SecretKeySpec((keyString).getBytes("UTF-8"), "HmacSHA1");
			Mac mac = Mac.getInstance("HmacSHA1");
			mac.init(key);

			byte[] bytes = mac.doFinal(msg.getBytes("ASCII"));

			StringBuffer hash = new StringBuffer();
			for (int i = 0; i < bytes.length; i++) {
				String hex = Integer.toHexString(0xFF & bytes[i]);
				if (hex.length() == 1) {
					hash.append('0');
				}
				hash.append(hex);
			}
			digest = hash.toString();
		} catch (UnsupportedEncodingException e) {
		} catch (InvalidKeyException e) {
		} catch (NoSuchAlgorithmException e) {
		}
		return digest;
	}

	/**
	 * current UTC time in format, which is accepted by HX 
	 * @return
	 */
	
	public String getUTCTime() {
		return DateFormatUtils.format(new Date(), "yyyy-MM-dd'T'HH:mm:ss", TimeZone.getTimeZone("UTC"));
	}

	/**
	 * Returns Access key and Access secret 
	 * Uses HX api method wsauth
	 * @return
	 * @throws ClientProtocolException
	 * @throws IOException
	 * @throws ParserConfigurationException
	 * @throws SAXException
	 */
	public String[] getAccessKeyAndSecret()
			throws ClientProtocolException, IOException, ParserConfigurationException, SAXException {
		String[] acessKeyAndSecret = new String[2];
		String time = getUTCTime();
		String wauthXML = String.format(
				"<?xml version='1.0'?> <hotelogix version='1.0' datetime='%s'><request method='wsauth' key='%s'></request></hotelogix>",
				time, CONSUMER_KEY);
		String signature = hmacDigest(CONSUMER_SECRET, wauthXML);

		CloseableHttpClient client = HttpClients.createDefault();
		HttpPost httpPost = new HttpPost("https://crs.staygrid.com/ws/web/wsauth");

		StringEntity entity = new StringEntity(wauthXML);
		httpPost.setEntity(entity);
		httpPost.setHeader("Content-type", "text/xml");
		httpPost.setHeader("X-HAPI-Signature", signature);
		CloseableHttpResponse response = client.execute(httpPost);
		String bodyAsString = EntityUtils.toString(response.getEntity());
		client.close(); // closes the client created
		
		// retrieve the value of access key and access secret
		acessKeyAndSecret[0] = bodyAsString.split("<accesskey value=")[1].split("/>")[0].replaceAll("\"", "");
		acessKeyAndSecret[1] = bodyAsString.split("<accesssecret value=")[1].split("/>")[0].replaceAll("\"", "");
		
		return acessKeyAndSecret;
	}

	/**
	 * Make call to HX api for method search, with checkindate and checkoutdate
	 * 
	 * @param checkindate
	 * @param checkoutdate
	 * @return
	 * @throws ClientProtocolException
	 * @throws IOException
	 * @throws ParserConfigurationException
	 * @throws SAXException
	 */
	public String getHXSearchResponse(String checkindate, String checkoutdate)
			throws ClientProtocolException, IOException, ParserConfigurationException, SAXException {
		
		String time = getUTCTime();
		String[] acessKeyAndSecret = getAccessKeyAndSecret();
		
		String searchXML = String.format(
				"<?xml version='1.0'?><hotelogix version='1.0' datetime='%s'><request method='search' key='%s'><stay checkindate='%s' checkoutdate='%s'/><pax adult='1' child='0' infant='0'/><roomrequire value='0'/><ignorelists><bookingpolicy/><cancellationpolicy/><roomtypeimage/><amenity/><rateimage/><inclusion/></ignorelists><limit value='0' offset='0' hasResult='0'/></request></hotelogix>",
				time, acessKeyAndSecret[0], checkindate, checkoutdate);

		String signature = hmacDigest(acessKeyAndSecret[1], searchXML);
		CloseableHttpClient client = HttpClients.createDefault();
		HttpPost httpPost = new HttpPost("https://crs.staygrid.com/ws/web/search");

		StringEntity entity = new StringEntity(searchXML);
		httpPost.setEntity(entity);
		httpPost.setHeader("Content-type", "text/xml");
		httpPost.setHeader("X-HAPI-Signature", signature);
		CloseableHttpResponse response = client.execute(httpPost);
		String bodyAsString = EntityUtils.toString(response.getEntity());

		client.close(); // closes the client created
		
		return bodyAsString; // returns the response body in xml 
	}

	public Map<String, Integer> hxRoomAvailability(String checkindate, String checkoutdate)
			throws ParserConfigurationException, SAXException, IOException {

		String[] churnedOutHotels = { "Canop Le Trans", "Midaas Comfort", "Alreef Residency", "Hotel Airport Grand",
				"Blossom Studios", "Epic", "Dummy", "Zipotel Silk Board" };
		String xml = getHXSearchResponse(checkindate, checkoutdate);
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		Document doc = dBuilder.parse(new ByteArrayInputStream(xml.getBytes()));
		doc.getDocumentElement().normalize();

		Map<String, Integer> hotelInfo = new ConcurrentSkipListMap<String, Integer>();

		NodeList nList = doc.getElementsByTagName("hotel");

		for (int temp = 0; temp < nList.getLength(); temp++) {
			Node nNode = nList.item(temp);
	
			if (nNode.getNodeType() == Node.ELEMENT_NODE) {
				Element eElement = (Element) nNode;
				String hotelName = eElement.getAttribute("title");
				if (!stringContainsItemFromArray(hotelName, churnedOutHotels)) {
					NodeList eList = eElement.getElementsByTagName("roomtype");
					if (eList.getLength() > 0) {
						for (int temp1 = 0; temp1 < eList.getLength(); temp1++) {
							Node eNode = eList.item(temp1);
							if (eNode.getNodeType() == Node.ELEMENT_NODE) {
								Element eElementOne = (Element) eNode;
								String roomType = eElementOne.getAttribute("title");
								int availableRoom = Integer.parseInt(eElementOne.getAttribute("availableroom"));
								hotelInfo.put(hotelName + "_" + roomType, availableRoom);
							}
						}
					}
				}
			}
		}

		return hotelInfo;
	}

	public Map<String, Integer> hxRoomAvailabilityByHotelName(String strHotelName, String checkindate,
			String checkoutdate) throws ParserConfigurationException, SAXException, IOException {

		String[] churnedOutHotels = { "Canop Le Trans", "Midaas Comfort", "Alreef Residency", "Hotel Airport Grand",
				"Blossom Studios", "Epic", "Dummy", "Zipotel Silk Board" };
		String xml = getHXSearchResponse(checkindate, checkoutdate);
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		Document doc = dBuilder.parse(new ByteArrayInputStream(xml.getBytes()));
		doc.getDocumentElement().normalize();

		Map<String, Integer> hotelInfo = new ConcurrentSkipListMap<String, Integer>();

		NodeList nList = doc.getElementsByTagName("hotel");

		for (int temp = 0; temp < nList.getLength(); temp++) {
			Node nNode = nList.item(temp);

			if (nNode.getNodeType() == Node.ELEMENT_NODE) {
				Element eElement = (Element) nNode;
				String hotelName = eElement.getAttribute("title");
				if ((hotelName.toLowerCase()).contains(strHotelName.toLowerCase())) {

					if (!stringContainsItemFromArray(hotelName, churnedOutHotels)) {
						NodeList eList = eElement.getElementsByTagName("roomtype");
						if (eList.getLength() > 0) {
							for (int temp1 = 0; temp1 < eList.getLength(); temp1++) {
								Node eNode = eList.item(temp1);

								if (eNode.getNodeType() == Node.ELEMENT_NODE) {
									Element eElementOne = (Element) eNode;
									String roomType = eElementOne.getAttribute("title");
									int availableRoom = Integer.parseInt(eElementOne.getAttribute("availableroom"));
									hotelInfo.put(hotelName + "_" + roomType, availableRoom);
								}
							}
						}
					}
				}
			}
		}

		return hotelInfo;
	}

	public Map<String, Set<String>> hxHotelPackagesAvailable(String checkindate, String checkoutdate)
			throws ParserConfigurationException, SAXException, IOException {

		String[] churnedOutHotels = { "Canop Le Trans", "Midaas Comfort", "Alreef Residency", "Hotel Airport Grand",
				"Blossom Studios", "Epic", "Dummy", "Zipotel Silk Board" };
		String xml = getHXSearchResponse(checkindate, checkoutdate);
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		Document doc = dBuilder.parse(new ByteArrayInputStream(xml.getBytes()));
		doc.getDocumentElement().normalize();

		Map<String, Set<String>> hotelInfo = new TreeMap<String, Set<String>>();

		NodeList nList = doc.getElementsByTagName("hotel");

		for (int temp = 0; temp < nList.getLength(); temp++) {
			Node nNode = nList.item(temp);

			if (nNode.getNodeType() == Node.ELEMENT_NODE) {
				Element eElement = (Element) nNode;
				String hotelName = eElement.getAttribute("title");

				if (!stringContainsItemFromArray(hotelName, churnedOutHotels)) {
					NodeList eList = eElement.getElementsByTagName("rate");
					if (eList.getLength() > 0) {
						Set<String> packageName = new TreeSet<String>();
						for (int temp1 = 0; temp1 < eList.getLength(); temp1++) {
							Node eNode = eList.item(temp1);

							if (eNode.getNodeType() == Node.ELEMENT_NODE) {
								Element eElementOne = (Element) eNode;
								packageName.add(eElementOne.getAttribute("title"));
								hotelInfo.put(hotelName, packageName);
							}
						}
					}
				}
			}
		}

		return hotelInfo;
	}

	public Map<String, Integer> getRoomTypeAndMaxPaxFromHX(String checkindate, String checkoutdate, String strHotelName)
			throws ParserConfigurationException, SAXException, IOException {

		String xml = getHXSearchResponse(checkindate, checkoutdate);
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		Document doc = dBuilder.parse(new ByteArrayInputStream(xml.getBytes()));
		doc.getDocumentElement().normalize();

		Map<String, Integer> roomTypeMaxPax = new TreeMap<String, Integer>();

		NodeList nList = doc.getElementsByTagName("hotel");

		for (int temp = 0; temp < nList.getLength(); temp++) {
			Node nNode = nList.item(temp);

			if (nNode.getNodeType() == Node.ELEMENT_NODE) {
				Element eElement = (Element) nNode;
				String hotelName = eElement.getAttribute("title");

				if (hotelName.contains(strHotelName)) {
					NodeList eList = eElement.getElementsByTagName("roomtype");
					if (eList.getLength() > 0) {

						for (int temp1 = 0; temp1 < eList.getLength(); temp1++) {
							Node eNode = eList.item(temp1);

							if (eNode.getNodeType() == Node.ELEMENT_NODE) {
								Element eElementOne = (Element) eNode;
								String roomType = eElementOne.getAttribute("title");
								int maxPax = Integer.parseInt(eElementOne.getAttribute("maxpax"));
								roomTypeMaxPax.put(roomType, maxPax);
							}
						}
					}
					break;
				}
			}
		}

		return roomTypeMaxPax;
	}

	public boolean stringContainsItemFromArray(String inputString, String[] items) {
		for (int i = 0; i < items.length; i++) {
			if (inputString.contains(items[i])) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Get the booking information stored in HX, using the orderId
	 */
	public String getOrderHXResponse(String orderId)
			throws ClientProtocolException, IOException, ParserConfigurationException, SAXException {
		String time = getUTCTime();

		String[] acessKeyAndSecret = getAccessKeyAndSecret();
		String getOrderXML = String.format(
				"<?xml version='1.0'?><hotelogix version='1.0' datetime='%s'><request method='getorder' key='%s'><orderId value='%s'/></request></hotelogix>",
				time, acessKeyAndSecret[0], orderId);
		// System.out.println(searchXML);
		String signature = hmacDigest(acessKeyAndSecret[1], getOrderXML);
		CloseableHttpClient client = HttpClients.createDefault();
		HttpPost httpPost = new HttpPost("https://crs.staygrid.com/ws/web/getorder");

		StringEntity entity = new StringEntity(getOrderXML);
		httpPost.setEntity(entity);
		httpPost.setHeader("Content-type", "text/xml");
		httpPost.setHeader("X-HAPI-Signature", signature);
		CloseableHttpResponse response = client.execute(httpPost);
		String bodyAsString = EntityUtils.toString(response.getEntity());
		client.close();
		return bodyAsString;
	}

	/**
	 * Cancel booking from HX using cancel API
	 */

	public void cancelBooking(String orderId, String reservationId)
			throws ClientProtocolException, IOException, ParserConfigurationException, SAXException {
		String time = getUTCTime();

		String[] acessKeyAndSecret = getAccessKeyAndSecret();
		String cancelXML = String.format(
				"<?xml version='1.0'?><hotelogix version='1.0' datetime='%s'><request method='cancel' key='%s'><orderId value='%s'/><reservationId value='%s'/><cancelCharge amount='0'/><cancelDescription>This is a test cancel from webservice</cancelDescription></request></hotelogix>",
				time, acessKeyAndSecret[0], orderId, reservationId);

		String signature = hmacDigest(acessKeyAndSecret[1], cancelXML);
		CloseableHttpClient client = HttpClients.createDefault();
		HttpPost httpPost = new HttpPost("https://crs.staygrid.com/ws/web/cancel");

		StringEntity entity = new StringEntity(cancelXML);
		httpPost.setEntity(entity);
		httpPost.setHeader("Content-type", "text/xml");
		httpPost.setHeader("X-HAPI-Signature", signature);
		CloseableHttpResponse response = client.execute(httpPost);
		String bodyAsString = EntityUtils.toString(response.getEntity());
		System.out.println(bodyAsString);
		client.close();
	}

	/**
	 * Parse the getOrder HX xml response to get following 1. fname 2. lname
	 * 3.email 4. mobile 5. orderAmount 6. depositTotal 7. paidAmount 8. Number
	 * of room bookings 9. For each room booking get -
	 * id,checkindate,checkoutdate,adult,child,code,groupcode,hotelname,
	 * statuscode
	 * 
	 * @throws SAXException 
	 * @throws ParserConfigurationException 
	 * @throws IOException 
	 * @throws ClientProtocolException 
	 */

	public Map<String, String> getOrderDetailsFromHX(String orderId) throws ClientProtocolException, IOException, ParserConfigurationException, SAXException {
		// Call HX api getorder
		String xml = getOrderHXResponse(orderId);
		System.out.println(xml);
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		Document doc = dBuilder.parse(new ByteArrayInputStream(xml.getBytes()));
		doc.getDocumentElement().normalize();
		
		Map<String,String> orderDetails = new HashMap<String,String>();
		
		NodeList nList = doc.getElementsByTagName("owner");
		for (int temp = 0; temp < nList.getLength(); temp++) {
			Node nNode = nList.item(temp);
			if (nNode.getNodeType() == Node.ELEMENT_NODE) {
				Element eElement = (Element) nNode;
				orderDetails.put("guest_first_name",eElement.getElementsByTagName("fname").item(0).getTextContent());
				orderDetails.put("guest_last_name",eElement.getElementsByTagName("lname").item(0).getTextContent());
				orderDetails.put("guest_email",eElement.getElementsByTagName("email").item(0).getTextContent());
				orderDetails.put("guest_mobile",eElement.getElementsByTagName("mobile").item(0).getTextContent());
			}
		}
		
		NodeList nListOne = doc.getElementsByTagName("orderamount");
		for (int temp = 0; temp < nListOne.getLength(); temp++) {
			Node nNodeOne = nListOne.item(temp);
			if (nNodeOne.getNodeType() == Node.ELEMENT_NODE) {
				Element eElement = (Element) nNodeOne;
				orderDetails.put("total_amount",eElement.getAttribute("amount"));
			}
		}
		
		NodeList nListTwo = doc.getElementsByTagName("booking");
		for (int temp = 0; temp < nListTwo.getLength(); temp++) {
			Node nNodeTwo = nListTwo.item(temp);
			if (nNodeTwo.getNodeType() == Node.ELEMENT_NODE) {
				Element eElement = (Element) nNodeTwo;
				orderDetails.put(String.format("checkindate_room%d", temp+1),eElement.getAttribute("checkindate"));
				orderDetails.put(String.format("checkoutdate_room%d", temp+1),eElement.getAttribute("checkoutdate"));
				orderDetails.put(String.format("adult_room%d", temp+1),eElement.getAttribute("adult"));
				orderDetails.put(String.format("child_room%d", temp+1),eElement.getAttribute("child"));
				orderDetails.put(String.format("statuscode_room%d", temp+1),eElement.getAttribute("statuscode"));
			}
		}
		
		NodeList nListThree = doc.getElementsByTagName("rate");
		for (int temp = 0; temp < nListThree.getLength(); temp++) {
			Node nNodeThree = nListThree.item(temp);
			if (nNodeThree.getNodeType() == Node.ELEMENT_NODE) {
				Element eElement = (Element) nNodeThree;
				orderDetails.put(String.format("pretax_room%d", temp+1),eElement.getAttribute("price"));
				orderDetails.put(String.format("tax_room%d", temp+1),eElement.getAttribute("tax"));
			}
		}
		
		NodeList nListFour = doc.getElementsByTagName("group");
		if (nListFour.getLength() == 0){
			nListFour = doc.getElementsByTagName("booking");
		}
		for (int temp = 0; temp < nListFour.getLength(); temp++) {
			Node nNodeFour = nListFour.item(temp);
			if (nNodeFour.getNodeType() == Node.ELEMENT_NODE) {
				Element eElement = (Element) nNodeFour;
				orderDetails.put("special_request",eElement.getElementsByTagName("preference").item(0).getTextContent());
			}
		}
		System.out.println(orderDetails.toString());
		return orderDetails;
	}

}
