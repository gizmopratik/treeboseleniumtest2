package base;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import java.util.HashMap;
import java.util.regex.Pattern;

/**
 * Lightweight wrapper around the JDBC API.
 */
public class DatabaseConnection {

  public static final String DB = "postgresql";
  private Connection _connection;


  /**
   * Constructor.
   * @param server            Server name (reserved "servers" may be used by different database drivers)
   * @param databaseName      Name of database
   * @throws SQLException     Error connecting to database
   */
  public DatabaseConnection(String server, String databaseName) throws SQLException {
    initConnection(server, databaseName, null);
  }

  /**
   * Constructor.
   *
   * @param server            Server name (reserved "servers" may be used by different database drivers)
   * @param databaseName      Name of database
   * @param user              User name
   * @param password          User password
   * @throws SQLException     Error connecting to database
   */
  public DatabaseConnection(String server, String databaseName,String user, String password) throws SQLException {
    Properties properties = new Properties();
    properties.put("user", user);
    properties.put("password", password);
    initConnection(server, databaseName, properties);
  }

  /**
   * Constructor.
   *
   * @param server            Server name (reserved "servers" may be used by different database drivers)
   * @param databaseName      Name of database
   * @param properties        Properties to pass to underlying database driver (driver dependent)
   * @throws SQLException     Error connecting to database
   */
  public DatabaseConnection(String server, String databaseName, Properties properties) throws SQLException {
    initConnection(server, databaseName, properties);
  }

  /**
   * Close this connection.
   *
   * @throws SQLException     Error closing connection.
   */
  public void close() throws SQLException {
    _connection.close();
  }

  /**
   * Execute a SQL statement.
   *
   * @param sql               SQL statement to execute
   * @throws SQLException     Error executing statement
   */
  public void execute(String sql) throws SQLException {
    logSql(sql);
    Statement stmt = createStatement();
    stmt.execute(sql);
    stmt.close();
  }

  /**
   * Execute a SQL query statement (SELECT).
   *
   * @param sql               SQL query statement to execute
   * @return                  QueryResults containing all results from query
   * @throws SQLException     Error executing query statement
   */
  public QueryResults executeQuery(String sql) throws SQLException {
    logSql(sql);
    Statement stmt = createStatement();
    ResultSet rs = stmt.executeQuery(sql);
    ResultSetMetaData rsmd = rs.getMetaData();
    QueryResults allResults = new QueryResults();
    while (rs.next()) {
      List<Object> results = new ArrayList<Object>();
      allResults.add(results);
      for (int i = 1; i <= rsmd.getColumnCount(); ++i) {
        results.add(rs.getObject(i));
      }
    }
    stmt.close();
    return allResults;
  }

  /**
   * Execute a SQL update statement (INSERT, UPDATE, DELETE).
   *
   * @param sql               SQL update statement to execute
   * @return                  Number of rows affected by the statement
   * @throws SQLException     Error executing update statement
   */
  public int executeUpdate(String sql) throws SQLException {
    logSql(sql);
    Statement stmt = createStatement();
    int numUpdated = stmt.executeUpdate(sql);
    stmt.close();
    return numUpdated;
  }

  /**
   * Execute a SELECT statement.
   *
   * @param columnNames       Column name(s) to retrieve from
   * @param tableName         Table name to retrieve from
   * @return                  QueryResults containing all results from SELECT
   * @throws SQLException     Error executing SELECT statement
   */
  public QueryResults select(String columnNames, String tableName) throws SQLException {
    return select(columnNames, tableName, null);
  }

  /**
   * Execute a SELECT statement.
   *
   * @param columnNames       Column name(s) to retrieve from
   * @param tableName         Table name to retrieve from
   * @param whereClause       Where clause for filtering results
   * @return                  QueryResults containing all results from SELECT
   * @throws SQLException     Error executing SELECT statement
   */
  public QueryResults select(String columnNames, String tableName, String whereClause) throws SQLException {
    String sql = String.format("SELECT %s FROM %s", columnNames, tableName);
    if (whereClause != null) {
      sql += " WHERE " + whereClause;
    }
    return executeQuery(sql);
  }

  /**
   * Execute an INSERT statement.
   *
   * @param tableName         Table name to insert values into
   * @param values            Values to insert
   * @return                  Number of rows inserted
   * @throws SQLException     Error executing INSERT statement
   */
  public int insert(String tableName, Object ... values) throws SQLException {
    if (values.length == 0) {
      throw new SQLException("No values to insert");
    }
    StringBuilder sql = new StringBuilder("INSERT INTO " + tableName + " VALUES(");
    for (int i = 0; i < values.length; ++i) {
      if (i > 0) {
        sql.append(", ");
      }
      sql.append(escape(values[i]));
    }
    sql.append(")");
    return executeUpdate(sql.toString());
  }

  /**
   * Execute an UPDATE statement.
   *
   * @param tableName         Table name to update
   * @param setValues         Values to set
   * @param whereClause       Where clause for determining which rows to update
   * @return                  Number of rows updated
   * @throws SQLException     Error executing UPDATE statement
   */
  public int update(String tableName, String setValues, String whereClause) throws SQLException {
    String sql = String.format("UPDATE %s SET %s WHERE %s", tableName, setValues, whereClause);
    return executeUpdate(sql);
  }

  /**
   * Execute a DELETE statement.
   *
   * @param tableName         Table name to update
   * @param whereClause       Where clause for determining which rows to delete
   * @return                  Number of rows deleted
   * @throws SQLException     Error executing DELETE statement
   */
  public int delete(String tableName, String whereClause) throws SQLException {
    String sql = String.format("DELETE FROM %s WHERE %s", tableName, whereClause);
    return executeUpdate(sql);
  }

  /**
   * Create a Statement object.
   *
   * @return                  A newly created statement object
   * @throws SQLException     Error creating statement object
   */
  public Statement createStatement() throws SQLException {
    return _connection.createStatement();
  }

  private void initConnection(String server, String databaseName, Properties properties) throws SQLException {
    String url = generateConnectionUrl(server, databaseName);
    if (properties != null) {
      try {
        _connection = DriverManager.getConnection(url, properties);
      } catch (SQLException e) {
        throw e;
      }
    } else {
      _connection = DriverManager.getConnection(url);
    }
  }

  private String generateConnectionUrl(String server, String databaseName) {
	    String databaseType = DB;
        return String.format("jdbc:%s://%s/%s", databaseType, server, databaseName);
  }

  private String escape(Object value) {
    if (value instanceof String) {
      return "'" + escapeSQL((String) value) + "'";
    }
    return value.toString();
  }

  private void logSql(String sql) {
	  //System.out.println("Executing SQL: " + sql);
  }
  
  /**
   * Replaces characters that may be confused by an SQL
   * parser with their equivalent escape characters.
   * <p>
   * Any data that will be put in an SQL query should
   * be be escaped.  This is especially important for data
   * that comes from untrusted sources such as Internet users.
   * <p>
   * For example if you had the following SQL query:<br>
   * <code>"SELECT * FROM addresses WHERE name='" + name + "' AND private='N'"</code><br>
   * Without this function a user could give <code>" OR 1=1 OR ''='"</code>
   * as their name causing the query to be:<br>
   * <code>"SELECT * FROM addresses WHERE name='' OR 1=1 OR ''='' AND private='N'"</code><br>
   * which will give all addresses, including private ones.<br>
   * Correct usage would be:<br>
   * <code>"SELECT * FROM addresses WHERE name='" + StringHelper.escapeSQL(name) + "' AND private='N'"</code><br>
   * <p>
   * Another way to avoid this problem is to use a PreparedStatement
   * with appropriate placeholders.
   *
   * @param s String to be escaped
   * @return escaped String
   * @throws NullPointerException if s is null.
   *
   * @since ostermillerutils 1.00.00
   */
  public String escapeSQL(String s){
    int length = s.length();
    int newLength = length;
    // first check for characters that might
    // be dangerous and calculate a length
    // of the string that has escapes.
    for (int i=0; i<length; i++){
      char c = s.charAt(i);
      switch(c){
        case '\\':
        case '\"':
        case '\'':
        case '\0':{
          newLength += 1;
        } break;
      }
    }
    if (length == newLength){
      // nothing to escape in the string
      return s;
    }
    StringBuffer sb = new StringBuffer(newLength);
    for (int i=0; i<length; i++){
      char c = s.charAt(i);
      switch(c){
        case '\\':{
          sb.append("\\\\");
        } break;
        case '\"':{
          sb.append("\\\"");
        } break;
        case '\'':{
          sb.append("\\\'");
        } break;
        case '\0':{
          sb.append("\\0");
        } break;
        default: {
          sb.append(c);
        }
      }
    }
    return sb.toString();
  }

  /**
   * Class containing results from query statements.
   */
  public static class QueryResults extends ArrayList<List<Object>> {

    private static final long serialVersionUID = 1L;

    public QueryResults() {
      super();
    }

    /**
     * Get the result object for a given row and column (0-based).
     *
     * @param row         Row to retrieve result from
     * @param column      Column to retrieve result from
     * @return            Object for the result at given location
     */
    public Object getResult(int row, int column) {
      return get(row).get(column);
    }

    /**
     * Get the number of rows.
     *
     * @return            Number of rows
     */
    public int getNumRows() {
      return size();
    }

    /**
     * Get the number of columns.
     *
     * @return            Number of columns
     */
    public int getNumCols() {
      if (getNumRows() == 0) {
        return 0;
      }
      return get(0).size();
    }
  }
}