package base;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.testng.log4testng.Logger;

/**
 * This class provides OS level utilities.
 */
public class OsUtils {

  private static Logger _logger = Logger.getLogger(OsUtils.class);

  private OsUtils() {
  }

  /**
   * Execute a command in a new process.  Waits for the execution to complete before returning.
   * This method if the process has a large stdout stream, it may block.
   *
   * @param command               Command to be executed
   * @return                      Process that's executing command
   * @throws IOException          if an I/O error occurs
   * @throws InterruptedException if the current thread is interrupted by another thread while it is waiting
   */
  public static Process execute(String command) throws IOException, InterruptedException {
    ProcessBuilder pb = new ProcessBuilder(command.split("\\s+"));
    Process p = pb.start();
    p.waitFor();
    return p;
  }

  /**
   * Execute a command in a new process.  Waits for the execution to complete before returning.  Return exit value.
   *
   * @param command               Command to be executed
   * @return                      Exit value of the executed command
   * @throws IOException          if an I/O error occurs
   * @throws InterruptedException if the current thread is interrupted by another thread while it is waiting
   */
  public static int executeWithNoOutput(String command) throws IOException, InterruptedException {
    Process p = execute(command);
    return p.exitValue();
  }

  /**
   * Execute a command in a new process.  Waits for the execution to complete before returning.  Return standard
   * output from command execution.
   *
   * @param command               Command to be executed
   * @return                      Standard output from command execution
   * @throws IOException          if an I/O error occurs
   * @throws InterruptedException if the current thread is interrupted by another thread while it is waiting
   */
  public static String executeWithStandardOutput(String command) throws IOException, InterruptedException {
    return execute(command.split("\\s+")).getStdout();
  }

  /**
   * Execute a command in a new process.  Waits for the execution to complete before returning.  Command is run
   * as an inline shell script (so allows things like piping).
   *
   * @param command               Command to be executed
   * @return                      Standard output from command execution
   * @throws IOException          if an I/O error occurs
   * @throws InterruptedException if the current thread is interrupted by another thread while it is waiting
   */
  public static String executeInlineShellScript(String command) throws IOException, InterruptedException {
    return execute(new String[] {"/bin/sh", "-c", command }).getStdout();
  }

  /**
   * Execute a command in a new process.  Waits for the execution to complete before returning.  Command is run
   * as an inline shell script (so allows things like piping).
   *
   * @param command               Command to be executed
   * @return                      Exit value of the executed command
   * @throws IOException          if an I/O error occurs
   * @throws InterruptedException if the current thread is interrupted by another thread while it is waiting
   */
  public static int executeInlineShellScriptWithNoOutput(String command) throws IOException, InterruptedException {
    ProcessWithStoredStdout p = execute(new String[] {"/bin/sh", "-c", command });
    return p.exitValue();
  }

  /**
   * Execute a command in a new process.  Runs command in parallel, so the underlying process might not be finished
   * by the time the method returns.  Command is run as an inline shell script (so allows things like piping).
   *
   * @param command               Command to be executed
   * @return                      Standard output from command execution
   * @throws IOException          if an I/O error occurs
   * @throws InterruptedException if the current thread is interrupted by another thread while it is waiting
   */
  public static Process executeInlineShellScriptInParallel(String command) throws IOException {
    return executeInlineShellScriptInParallel(command, false, false, false);
  }

  /**
   * Execute a command in a new process.  Runs command in parallel, so the underlying process might not be finished
   * by the time the method returns.  Command is run as an inline shell script (so allows things like piping).
   *
   * @param command               Command to be executed
   * @param inheritStdin          Whether to inherit the parent's process stdin
   * @param inheritStdout         Whether to inherit the parent's process stdout
   * @param inheritStderr         Whether to inherit the parent's process stderr
   * @return                      Standard output from command execution
   * @throws IOException          if an I/O error occurs
   * @throws InterruptedException if the current thread is interrupted by another thread while it is waiting
   */
  public static Process executeInlineShellScriptInParallel(String command, boolean inheritStdin, boolean inheritStdout,
      boolean inheritStderr) throws IOException {
    ProcessBuilder pb = new ProcessBuilder(new String[] {"/bin/sh", "-c", command});
    if (inheritStdin) {
      pb.redirectInput(ProcessBuilder.Redirect.INHERIT);
    }
    if (inheritStdout) {
      pb.redirectOutput(ProcessBuilder.Redirect.INHERIT);
    }
    if (inheritStderr) {
      pb.redirectError(ProcessBuilder.Redirect.INHERIT);
    }
    Process p = pb.start();
    return p;
  }

  /**
   * Execute a command in a new process.  Waits for the execution to complete before returning.  Return exit value.
   * Command is run as an inline shell script (so allows things like piping).
   *
   * @param command               Command to be executed
   * @param stdout                Buffer to write stdout to
   * @param stderr                Buffer to write stderr to
   * @return                      Exit value of the executed command
   * @throws IOException          if an I/O error occurs
   * @throws InterruptedException if the current thread is interrupted by another thread while it is waiting
   */
  public static int executeWithStandardOutputAndError(String command, StringBuilder stdout
      , StringBuilder stderr) throws IOException, InterruptedException {
    Process p = execute(command);
    if (stdout != null) {
      stdout.append(getStdout(p));
    }
    if (stderr != null) {
      stderr.append(getStderr(p));
    }
    return p.exitValue();
  }

  /**
   * Execute a command in a new process.  Waits for the execution to complete before returning.  Return exit value.
   *
   * @param command               Command to be executed
   * @param stdout                Buffer to write stdout to
   * @param stderr                Buffer to write stderr to
   * @return                      Exit value of the executed command
   * @throws IOException          if an I/O error occurs
   * @throws InterruptedException if the current thread is interrupted by another thread while it is waiting
   */
  public static int executeInlineShellScriptWithStandardOutputAndError(String command, StringBuilder stdout
      , StringBuilder stderr) throws IOException, InterruptedException {
    ProcessWithStoredStdout p = execute(new String[] {"/bin/sh", "-c", command });
    if (stdout != null) {
      stdout.append(p.getStdout());
    }
    if (stderr != null) {
      stderr.append(p.getStderr());
    }
    return p.exitValue();
  }

//  /**
//   * Kill process by name by getting pid and executing 'kill -9 pid'
//   * Workaround the problem that 'pkill -9 processName' does not work as expected
//   *
//   * @param processName     Process name to kill
//   * @return                True if process found and killed
//   */
//  public static boolean killProcess(String processName) {
//    try {
//      String cmd = "ps -ef | grep -E '( |/)" + processName + "( |$)' | grep -v grep | awk '{print $2}'";
//      String pid = OsUtils.executeInlineShellScript(cmd);
//      if (!pid.isEmpty()) {
//        _logger.info("Killing the " + processName + " process " + pid);
//        cmd = "kill -9 " + pid;
//        return OsUtils.executeWithNoOutput(cmd) == 0;
//      }
//    } catch (IOException | InterruptedException e) {
//      _logger.error("Unable to kill " + processName + " process: " + e.getMessage());
//    }
//    return false;
//  }

  /**
   * Get process count matching the processName
   * @param processName
   * @return                        Number of processes matching the name
   * @throws IOException
   * @throws InterruptedException
   */
  public static int getProcessCount(String processName) throws IOException, InterruptedException {
    String cmd = "ps -fA | grep \"" + processName + "\"";
    String[] lines = executeInlineShellScript(cmd).split("\n");
    return lines.length - 2;
  }

  /**
   * Get the executable path matching the resource name given
   * @param resourceName            Name of the executable to find
   * @return                        Absolute path of the first executable matched in the PATH or null
   */
  public static String getExecutableInPath(String resourceName) {
    for (String dir : System.getenv("PATH").split(":")) {
      File file = new File(dir);
      if (file.isDirectory() && new File(file, resourceName).exists()) {
        return new File(file, resourceName).getAbsolutePath();
      } else if (file.canExecute() && file.getName().equals(resourceName)) {
        return file.getAbsolutePath();
      }
    }
    return null;
  }

  private static ProcessWithStoredStdout execute(String[] commands) throws IOException, InterruptedException {
    ProcessBuilder pb = new ProcessBuilder(commands);
    Process p = pb.start();
    ProcessWithStoredStdout process = new ProcessWithStoredStdout(p);
    process.waitFor();
    return process;
  }

  private static String getStdout(Process p) throws IOException {
    return getInputStreamAsString(p.getInputStream());
  }

  private static String getStderr(Process p) throws IOException {
    return getInputStreamAsString(p.getErrorStream());
  }

  private static String getInputStreamAsString(InputStream is) throws IOException {
    StringBuilder sb = new StringBuilder();
    BufferedReader br = new BufferedReader(new InputStreamReader(is, "UTF-8"));
    boolean firstLine = true;
    String line;
    while ((line = br.readLine()) != null) {
      if (!firstLine) {
        sb.append("\n");
      }
      firstLine = false;
      sb.append(line);
    }
    return sb.toString();
  }

  private static class ProcessWithStoredStdout {

    private Process _process;
    private BufferedReader _out;
    private StringBuilder _stdoutBuffer;

    public ProcessWithStoredStdout(Process process) {
      this._process = process;
      _out = new BufferedReader(new InputStreamReader(_process.getInputStream()));
      _stdoutBuffer = new StringBuilder();
    }

    public Process getProcess() {
      return _process;
    }

    public int waitFor() throws IOException, InterruptedException {
      boolean firstLine = true;
      String line;
      while ((line = _out.readLine()) != null) {
        if (!firstLine) {
          _stdoutBuffer.append("\n");
        }
        firstLine = false;
        _stdoutBuffer.append(line);
      }
      return _process.waitFor();
    }

    public String getStdout() {
      return _stdoutBuffer.toString();
    }

    public String getStderr() throws IOException {
      return getInputStreamAsString(_process.getErrorStream());
    }

    public int exitValue() {
      return _process.exitValue();
    }

  }

}