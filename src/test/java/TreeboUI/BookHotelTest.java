package TreeboUI;

import org.testng.AssertJUnit;
import org.testng.AssertJUnit;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import PageFactory.HomePage;
import PageFactory.ITCPage;
import PageFactory.LoginPage;
import DataProvider.DataProviderClass;
import org.testng.Assert;

public class BookHotelTest {

	WebDriver driver;
	LoginPage objLogin;
	HomePage objHome;
	ITCPage objITC;
	String str;

	@BeforeTest()
	public void setup() {
		// Chrome Driver
		 System.setProperty("webdriver.chrome.driver", "/usr/local/share/chromedriver");
		// System.setProperty("webdriver.chrome.driver", "/Users/ashishmantri/Downloads/chromedriver");
		driver = new ChromeDriver();
		// driver = new FirefoxDriver();
		// driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

		driver.get("http://treebohotels.com");
		driver.manage().window().maximize();
	}

	/**
	 * This test go to http://treebohotels.com Verify login page title as
	 * toashishmantriATgmail.com Login to application
	 **/

	// @Parameters({ "username", "password" })
	@Test(priority = 0)
	public void test_book_a_hotel() throws InterruptedException {

		// Create Login Page object
		objLogin = new LoginPage(driver);
		objHome = new HomePage(driver);
		objITC = new ITCPage(driver);
		WebDriverWait wait = new WebDriverWait(driver, 10);

		// login to application
		objLogin.loginToTreebo("treebotest@gmail.com", "password");

		// Verify if the search button is present or not.
		AssertJUnit.assertEquals(objHome.checksearchbutton(), true);
		AssertJUnit.assertEquals(driver.getTitle(), "Treebo | Home");

		driver.findElement(By.className("inputText")).sendKeys("Treebo Akshaya Mayflower");

		// Do an Empty search
		objHome.clickSearchButton();

		// Verify if user reaches on the search page.
		AssertJUnit.assertEquals(driver.getTitle(), "Treebo | Search");

		Thread.sleep(30000);
		driver.findElement(By.className("pb20")).click();

		// Click on view details for the first hotel and click on room
		// driver.findElement(By.id("searchedHotelsDetail")).click();
		// driver.findElement(By.linkText("View Details")).click();
		Thread.sleep(5000);
		driver.findElement(By.id("searchedHotelsName")).click();

		// CLick on 3rd button (book a treebo)
		driver.findElement(By.name("hotelBookNow")).click();

		// Verify if user reaches on the Itinerary Section.
		AssertJUnit.assertEquals(driver.getTitle(), "Treebo | Confirmation");
		Thread.sleep(30000);
		driver.findElement(By.id("login-continue")).click();

		// Verify if user reaches on the Travelers Section.
		Thread.sleep(30000);
		AssertJUnit.assertEquals(driver.getTitle(), "Treebo | Traveller");
		driver.findElement(By.className("pay_at_hotel")).click();

		// Verify if user reaches on the Confirmation Section.
		Thread.sleep(30000);
		AssertJUnit.assertEquals(driver.getTitle(), "Treebo | Order");
	}

	// @AfterTest()
	// Close browser session
	// public void teardown() throws InterruptedException {

	// Cancel the booking
	// objITC.cancelBooking();
	// driver.close();
	// }
}
