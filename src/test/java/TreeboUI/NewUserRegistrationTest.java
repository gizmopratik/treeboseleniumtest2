package TreeboUI;


import javax.swing.text.StyledEditorKit.StyledTextAction;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.AssertJUnit;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import PageFactory.LoginPage;
import PageFactory.NewUserRegistrationPage;
import DataProvider.DataProviderClass;


public class NewUserRegistrationTest {

	WebDriver driver;
	NewUserRegistrationPage objNewUserRegistration;

	@BeforeTest()
	public void setup() {
		// Chrome Driver
		System.setProperty("webdriver.chrome.driver", "/Users/ashishmantri1/Downloads/chromedriver");

		//driver = new ChromeDriver();
		 driver = new FirefoxDriver();
		// driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

		driver.get("http://treebohotels.com");
		driver.manage().window().maximize();
	}

	/**
	 * This test go to http://treebohotels.com Verify login page title as
	 * toashishmantriATgmail.com Login to application
	 **/

	@Test(priority = 0)
	public void test_new_user_registration() {

		// Create Login Page object
		objNewUserRegistration = new NewUserRegistrationPage(driver);
		AssertJUnit.assertEquals(true, driver.findElements(By.name("headerLogout")).size() == 0);		
		// Click on Registration Link
		driver.findElement(By.name("headerRegister")).click();

		objNewUserRegistration.createNewUser("John Dow", "9876543210", objNewUserRegistration.generateRandomEmail(), "treebo1234");
	
		// Verify if the search button is present or not.
		AssertJUnit.assertEquals(driver.getTitle(), "Treebo | Home");
		AssertJUnit.assertEquals(true, driver.findElements(By.name("headerLogout")).size() != 0);
		
				
				

		
		
	}
}
