package desktopUITest;

import java.text.ParseException;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import utils.CommonUtils;
import base.BrowserHelper;
import base.TestBaseSetUp;
import desktopUIPageObjects.HomePage;
import desktopUIPageObjects.Account;

public class AccountTest extends TestBaseSetUp {

	private HomePage homePage;
    private Account account;
    private BrowserHelper browserHelper;
	
	/**
	 * This test verifies that profile fields can be updated
	 */
	@Test(groups = { "Sanity", "Account" })
	public void testProfileFieldUpdates() throws ParseException {
		homePage = new HomePage();
        account = new Account();
		CommonUtils utils = new CommonUtils();
		SoftAssert verify = new SoftAssert();
		
		String guestEmail = utils.getProperty("TestAccountLogin"); // "treebotestuiauto@gmail.com";
		String guestPassword = utils.getProperty("TestLoginPassword"); // "password";
		
		String firstNameToUpdate = "UpdatedTest";
		String lastNameToUpdate = "UpdatedLast";
		String mobileToUpdate = "5000000000";
		
		// Home Page is Displayed
		homePage.verifyHomePagePresence();
		homePage.signInAsTreeboMember(guestEmail, guestPassword);
		
		//go to profile page
		account.goToProfilePage();
		
		// Update Name, last name and Mobile
		//Get current values
		String origFirstName = account.getProfileFieldValue("first_name");
		String origLastName =  account.getProfileFieldValue("last_name");
		String origMobile = account.getProfileFieldValue("phone");
		
		//Now Update values
		account.updateNameAndMobile(firstNameToUpdate, lastNameToUpdate, mobileToUpdate);
		
		// Go to Home Page and come back to check new data
		homePage.goToHomePage();
		
		//Go to profile 
		account.goToProfilePage();
		//Get updated values
		String updatedFirstName = account.getProfileFieldValue("first_name");
		String updatedLastName =  account.getProfileFieldValue("last_name");
		String updatedMobile = account.getProfileFieldValue("phone");
		
		//Now assert
		verify.assertEquals(updatedFirstName, firstNameToUpdate);
		verify.assertEquals(updatedLastName, lastNameToUpdate);
		verify.assertEquals(updatedMobile, mobileToUpdate);
		
		// Sign out and sign in and then verify new data persists
		account.doSignOut();
		homePage.signInAsTreeboMember(guestEmail, guestPassword);
		account.goToProfilePage();
		
		//Get updated values
		String updatedFirstName1 = account.getProfileFieldValue("first_name");
		String updatedLastName1 =  account.getProfileFieldValue("last_name");
		String updatedMobile1 = account.getProfileFieldValue("phone");
		
		//Now assert
		verify.assertEquals(updatedFirstName1, firstNameToUpdate);
		verify.assertEquals(updatedLastName1, lastNameToUpdate);
		verify.assertEquals(updatedMobile1, mobileToUpdate);
		
		//reset
		account.updateNameAndMobile(origFirstName, origLastName, origMobile);
		homePage.goToHomePage();
		account.goToProfilePage();
		String updatedFirstName2 = account.getProfileFieldValue("first_name");
		//Now assert
		verify.assertEquals(updatedFirstName2, origFirstName);
		verify.assertAll();// finally assert All
	}
	
	/**
	 * This test verifies password can be reset 
	 */
	@Test(groups = { "Sanity", "Account" })
	public void testAccountPaaswordReset() throws ParseException {
		homePage = new HomePage();
        account = new Account();
        browserHelper = new BrowserHelper();
		CommonUtils utils = new CommonUtils();

		String guestEmail = utils.getProperty("TestAccountPwdReset"); // "treebotestuiauto@gmail.com";
		String guestPassword = utils.getProperty("TestLoginPassword"); // "password";
		String newPassword = "password1";

		// Home Page is Displayed
		homePage.verifyHomePagePresence();
		try{
			homePage.signInAsTreeboMember(guestEmail, guestPassword);
			// Go to Settings page
			account.goToSettingsPage();
			account.changePassword(guestPassword, newPassword);
			
			account.doSignOut();
			
			//Now check if can login with new password
			homePage.signInAsTreeboMember(guestEmail, newPassword);
			
			// Now reset the password to original 
			account.goToSettingsPage();
			account.changePassword(newPassword,guestPassword);
			account.doSignOut();
			homePage.signInAsTreeboMember(guestEmail, guestPassword);
		}catch(Exception e){
			browserHelper.browserRefresh();
			guestPassword = "password1";
			newPassword = utils.getProperty("TestLoginPassword");
			homePage.signInAsTreeboMember(guestEmail, guestPassword);
			// Go to Settings page
			account.goToSettingsPage();
			account.changePassword(guestPassword, newPassword);
			
			account.doSignOut();
			
			//Now check if can login with new password
			homePage.signInAsTreeboMember(guestEmail, newPassword);
			
			// Now reset the password to original 
			account.goToSettingsPage();
			account.changePassword(newPassword,guestPassword);
			account.doSignOut();
			homePage.signInAsTreeboMember(guestEmail, guestPassword);
		}
	}
}
