package desktopUITest;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;

import org.apache.http.client.ClientProtocolException;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import org.xml.sax.SAXException;

import base.HotelLogixAPI;
import base.TestBaseSetUp;
import mobileSiteUIPageObjects.HomeScreen;
import utils.CommonUtils;
import utils.DBUtils;

public class CancelBooking {
	
	@Test(groups={"CancelBooking"})
	public void testMobileLoginAtHomePage() throws SQLException, ClientProtocolException, IOException, ParserConfigurationException, SAXException{
		DBUtils db = new DBUtils();
		HotelLogixAPI api = new HotelLogixAPI();
		
		Map<String,String> openBookingInfo = db.getOrderIdAndReservationId("toashishmantri@gmail.com", "2016-06-23");
		for (String key : openBookingInfo.keySet()){
			api.cancelBooking(key, openBookingInfo.get(key));
		}
	}
}
