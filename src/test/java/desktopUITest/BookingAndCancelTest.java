package desktopUITest;

import java.text.ParseException;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import utils.CommonUtils;
import base.TestBaseSetUp;
import desktopUIPageObjects.HomePage;
import desktopUIPageObjects.BookingHistory;
import desktopUIPageObjects.HotelResultsPage;
import desktopUIPageObjects.ItineraryPage;
import desktopUIPageObjects.ConfirmationPage;
import base.BrowserHelper;

public class BookingAndCancelTest extends TestBaseSetUp {

	private HomePage homePage;
	private HotelResultsPage hotelResultsPage;
	private ItineraryPage itineraryPage;
	private ConfirmationPage confirmationPage;
	private BookingHistory bookingHistory;
	private BrowserHelper browserHelper;

	/***
	 * Book a treebo hotel by registering to the website in Itinerary page
	 * Cancel the booking
	 * 
	 * @throws InterruptedException
	 */
	@Test(groups = { "HotelBooking", "Sanity" })
	public void testHotelBookingAndCancellationThroughQuickBookPayAtHotelSignUpInItineraryPage()
			throws ParseException, InterruptedException {
		homePage = new HomePage();
		hotelResultsPage = new HotelResultsPage();
		itineraryPage = new ItineraryPage();
		confirmationPage = new ConfirmationPage();
		browserHelper = new BrowserHelper();
		bookingHistory = new BookingHistory();
		CommonUtils utils = new CommonUtils();
		SoftAssert verify = new SoftAssert();

		String location = utils.getProperty("Country"); // "India";
		String testHotel = utils.getProperty("DummyHotel"); // "Dummy";
		String testUrl = utils.getProperty("TestUrl");
		int hotelIndex = 0;
		int checkInFromCurrentDate = utils.getRandomNumber(30, 45);
		int checkOutFromCurrentDate = checkInFromCurrentDate + 1;
		int roomIndex = 1;
		String[] checkInCheckOutDates = new String[2];

		// Home Page is Displayed
		homePage.verifyHomePagePresence();
		// Do search and get checkin checkout dates
		checkInCheckOutDates = homePage.doSearch(location, checkInFromCurrentDate, checkOutFromCurrentDate, roomIndex);
		hotelResultsPage.verifyHotelResultsPagePresence();

		if (browserHelper.getCurrentUrl().contains(testUrl)) {
			hotelIndex = hotelResultsPage.getHotelIndexByHotelName(testHotel);
		} else {
			hotelIndex = hotelResultsPage.getHotelIndexByHotelAvailable();
		}

		String hotelName = hotelResultsPage.getHotelNameByIndex(hotelIndex);
		String hotelRoomRate = hotelResultsPage.getHotelRoomRateByIndex(hotelIndex);
		String hotelAddress = hotelResultsPage.getHotelAddressByIndex(hotelIndex);
		System.out.println(
				"Hotel Name:" + hotelName + " Hotel room rate : " + hotelRoomRate + " Hotel Address:" + hotelAddress);

		hotelResultsPage.clickQuickBookByIndex(hotelIndex);
		itineraryPage.verifyItineraryPagePresence();
		String[] guestDetails = itineraryPage.signupInItineraryPage();
		Thread.sleep(120000);
		itineraryPage.bookWithPayAtHotel();

		confirmationPage.verifyConfirmationPagePresence();
		String bookingId = confirmationPage.getBookingId();

		verify.assertEquals(guestDetails[0], confirmationPage.getGuestNameInConfPage());
		verify.assertEquals(guestDetails[1], confirmationPage.getGuestEmailInConfPage());
		verify.assertEquals(guestDetails[2], confirmationPage.getGuestMobileInConfPage());
		verify.assertEquals(hotelName, confirmationPage.getHotelNameInConfirmationPage());

		System.out.println(utils.formatDate(checkInCheckOutDates[0]).toUpperCase());
		System.out.println(confirmationPage.getHotelCheckInDateInConfirmationPage().toUpperCase());
		verify.assertEquals(utils.formatDate(checkInCheckOutDates[0]).toUpperCase(),
				confirmationPage.getHotelCheckInDateInConfirmationPage().toUpperCase());
		verify.assertEquals(utils.formatDate(checkInCheckOutDates[1]).toUpperCase(),
				confirmationPage.getHotelCheckOutDateInConfirmationPage().toUpperCase());

		bookingHistory.cancelAllBookings();
		verify.assertFalse(bookingHistory.isConfirmedBookingAvailable(),
				"Booking should have been cancelled! " + " Email : " + guestDetails[1]);

		verify.assertAll();
	}
}