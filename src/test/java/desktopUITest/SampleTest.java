package desktopUITest;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.xml.sax.SAXException;

import com.google.common.collect.Maps;

import base.BrowserHelper;
import base.DatabaseConnection;
import base.HotelLogixAPI;
import base.OsUtils;
import base.TestBaseSetUp;
import utils.CommonUtils;
import utils.DBUtils;
import desktopUIPageObjects.HomePage;
import desktopUIPageObjects.HotelResultsPage;
import desktopUIPageObjects.ItineraryPage;
import desktopUIPageObjects.ConfirmationPage;
import desktopUIPageObjects.HotelDetailsPage;
import desktopUIPageObjects.BookingHistory;
import desktopUIPageObjects.Account;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;

public class SampleTest extends TestBaseSetUp {

	private HomePage homePage;
	private HotelResultsPage hotelResultsPage;
	private ItineraryPage itineraryPage;
	private ConfirmationPage confirmationPage;
	private HotelDetailsPage hotelDetailsPage;
	private BookingHistory bookingHistory;
	private BrowserHelper browserHelper;
	private Account account;

	/**
	 * This test verifies the Login as Treebo member from Home Page
	 * @throws InterruptedException 
	 * @throws IOException 
	 * @throws SQLException 
	 * @throws SAXException 
	 * @throws ParserConfigurationException 
	 * @throws FileNotFoundException 
	 */
	@Test(groups = { "Sample" },enabled = false)
	public void testSample() throws IOException, InterruptedException, SQLException, ParserConfigurationException, SAXException {
		HotelLogixAPI api = new HotelLogixAPI();
		DBUtils db = new DBUtils();
		
		Map<String,Integer> hxAvailability = api.hxRoomAvailability("2016-04-28","2016-04-29");
		Map<String,Integer> dbAvailaiblity = db.getRoomAvailability("2016-04-28");
		//System.out.println(api.hxRoomAvailability("2016-05-29","2016-05-30").keySet().toString());
		
		Maps.difference(hxAvailability,dbAvailaiblity);
		//System.out.println(db.getRoomAvailability("2016-05-29").keySet().toString());
		System.out.println(Maps.difference(hxAvailability,dbAvailaiblity).toString());
		//System.out.println(y);
//		String filePath = "";
//		System.out.println("***filepath***" + filePath);
//		String strCommand = String.format("/usr/bin/python %s '2016-05-30' '2016-05-31'", filePath);
//		System.out.println("***filepath***" + strCommand);
//		String y = OsUtils.executeInlineShellScript("which python");
//		String z = OsUtils.executeWithStandardOutput("/usr/bin/python --version");
//		System.out.println("******" + y);
//		System.out.println("******" + z);
//		String x = OsUtils.executeInlineShellScript(strCommand);
//		System.out.println(OsUtils.executeInlineShellScript("/usr/bin/python --version"));
//		System.out.println(x);
		
//		  CommonUtils utils = new CommonUtils();
//		  String server = "172.40.20.210";
//		  String databaseName = "ashishmantri";
//		  String user = "ashishmantri";
//	      String PGPASSWORD = "ashishmantri";
////	      
//////		  String server = "treebo-pg-master.cadmhukcksta.ap-southeast-1.rds.amazonaws.com";
//////		  String databaseName = "treebo";
//////		  String user = "treeboadmin";
//////	      String PGPASSWORD = "caTwimEx3";
////	      
////	      
//	      String date = "2016-04-30";
//	     
//	      String sql = String.format("SELECT hotelogix_name,room_type,availablerooms FROM bookingstash_availability INNER JOIN hotels_room ON bookingstash_availability.room_id = hotels_room.id and bookingstash_availability.date = '%s' INNER JOIN hotels_hotel ON hotels_hotel.id = hotel_id order by hotelogix_name asc", date);
//		  //String sql = "select name from hotels_hotel where id =78;";
////		     
//	      System.out.println(sql);
//	      DatabaseConnection dc = new DatabaseConnection(server,databaseName,user,PGPASSWORD);
//	      Iterator itr = dc.executeQuery(sql).iterator();
//	      Map<String,Integer> dbRoomAvailability = new HashMap<String,Integer>();
//	      while (itr.hasNext()){
//	    	   String[] strArr = itr.next().toString().split(",");
//	    	   String hotel_name = strArr[0].replaceAll("\\[", "").trim();
//	    	   String room_type = strArr[1].trim();
//	    	   int available = Integer.parseInt(strArr[2].replaceAll("\\]", "").trim());
//	    	   dbRoomAvailability.put(hotel_name+"_"+ room_type, available);
//	      }
//	      System.out.println(dbRoomAvailability.keySet().toString());
	      
	      //System.out.println(dc.executeQuery(sql));
	      //System.out.println(dc.executeQuery(sql));
//	      dc.close();
//		
//		homePage = new HomePage(driver);
//		hotelResultsPage = new HotelResultsPage(driver);
//		CommonUtils utils = new CommonUtils();
//		
//		int checkInFromCurrentDate = 31;
//		int checkOutFromCurrentDate = checkInFromCurrentDate + 1;
//		int[][] roomIndex = {{1,2,1},{2,2,2}};
//		homePage.doSearch("India", checkInFromCurrentDate, checkOutFromCurrentDate, roomIndex);
//		int hotelIndex = hotelResultsPage.getHotelIndexByHotelAvailable();
//		hotelResultsPage.clickQuickBookByIndex(hotelIndex);
//		itineraryPage.verifyItineraryPagePresence();
//		itineraryPage.clickContinueAsGuest();
		//Assert.assertTrue(false);
//		System.out.println(hotelResultsPage.getHotelIndexByHotelAvailable());
//		
//		homePage = new HomePage(driver);
//		account = new Account(driver);
//		hotelResultsPage = new HotelResultsPage(driver);
//		itineraryPage = new ItineraryPage(driver);
//		confirmationPage = new ConfirmationPage(driver);
//		hotelDetailsPage = new HotelDetailsPage(driver);
//		bookingHistory = new BookingHistory(driver);
//		browserHelper = new BrowserHelper(driver);
//		CommonUtils utils = new CommonUtils();
		
		
		
		

//		homePage.signInAsTreeboMember("treebotestaccount@gmail.com", "password1");
//        account.goToProfilePage();
//		//System.out.println(driver.findElement(By.cssSelector("#profileDetailData")).getAttribute("value"));
//		System.out.println(account.returnProfileData().get("email"));
		
		//driver.findElement(By.cssSelector("#firstName")).clear();
		
//		String location = utils.getProperty("Country"); // "India";
//		String testHotel = utils.getProperty("DummyHotel"); // "Dummy";
//		//int hotelIndex = 0;
//		int checkInFromCurrentDate = utils.getRandomNumber(120, 130);
//		int checkOutFromCurrentDate = checkInFromCurrentDate + 1;
//		int roomIndex = 1;
//
//		
//		//System.out.println(utils.getDaysDiffFromSelectedDateToCurrentDate("4/20/2016"));
//		homePage.doSearch();
//		hotelResultsPage.clickHotelNameByIndex(9);
//		//hotelDetailsPage.ensureRoomAvailableInHDPage();
//		hotelDetailsPage.getHotelPriceBreakUpInHD();
//		//hotelDetailsPage.clickBookNowInHotelsDetailsPage();
//		//itineraryPage.getHotelPriceInfoInItineraryPage();
//		//hotelDetailsPage.applyCouponInHDPage("APR15");
		
//
//		homePage.verifyHomePagePresence();
//        System.out.println("Index :" + hotelIndex);
//        if ( hotelIndex == 3){
//        	Assert.assertTrue(false);
//        }
//        
//        if ( hotelIndex == 1){
//        	Assert.assertTrue(false);
//        }
//		
//		File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
//		
//		FileUtils.copyFile(scrFile, new File("screenshot.png"));
		
//		homePage.doSearch(location, checkInFromCurrentDate, checkOutFromCurrentDate, roomIndex);
//		hotelResultsPage.verifyHotelResultsPagePresence();
//		int roomRateInSearch = Integer.parseInt(hotelResultsPage.getHotelRoomRateByIndex(hotelIndex));
//		Assert.assertTrue(hotelResultsPage.isTotalInBreakUpAndRoomRateEqual(hotelIndex));
//		Assert.assertTrue(hotelResultsPage.isRoomPriceCalcultaionCorrect(hotelIndex));
//		
//		hotelResultsPage.clickHotelNameByIndex(hotelIndex);
//		Assert.assertTrue(hotelDetailsPage.isPriceCalcultaionInBreakUpCorrectInHDPage());
//		Assert.assertTrue(hotelDetailsPage.isTotalInBreakUpAndRoomRateEqualInHDPage());
//		int hotelRoomRateInHD = hotelDetailsPage.getHotelTotalPriceInHD();
//		Assert.assertTrue(roomRateInSearch == hotelRoomRateInHD);
//		
//		hotelDetailsPage.clickBookNowInHotelsDetailsPage();
//		
//		Assert.assertTrue(itineraryPage.isRoomPriceCalcultaionCorrectInItineraryPage());
//		int grandTotalItineraryPage = itineraryPage.getHotelPriceInfoInItineraryPage()[3];
//		Assert.assertTrue(hotelRoomRateInHD == grandTotalItineraryPage);
//		Assert.assertTrue(false);
		
//		File scrFile1 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
//		FileUtils.copyFile(scrFile1, new File("screenshot1.png"));

//		if (browserHelper.getCurrentUrl().contains("staging")) {
//			hotelIndex = hotelResultsPage.getHotelIndexByHotelName(testHotel);
//		} else {
//			hotelIndex = hotelResultsPage.getHotelIndexByHotelAvailable();
//		}
//
//		String hotelName = hotelResultsPage.getHotelNameByIndex(hotelIndex);
//		String hotelRoomRate = hotelResultsPage.getHotelRoomRateByIndex(hotelIndex);
//		String hotelAddress = hotelResultsPage.getHotelAddressByIndex(hotelIndex);
//		System.out.println(
//				"Hotel Name:" + hotelName + " Hotel room rate : " + hotelRoomRate + " Hotel Address:" + hotelAddress);
//
//		hotelResultsPage.clickHotelNameByIndex(hotelIndex);
//		File scrFile2 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
//		FileUtils.copyFile(scrFile2, new File("screenshot2.png"));
//		hotelDetailsPage.clickBookNowInHotelsDetailsPage();
//		File scrFile3 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
//		FileUtils.copyFile(scrFile3, new File("screenshot3.png"));
//		itineraryPage.verifyItineraryPagePresence();
//		Assert.assertTrue(false);
		
		
		
		
//		CommonUtils utils = new CommonUtils();
//		String email = utils.getProperty("TestLoginEmail");  
//		String password = utils.getProperty("TestLoginPassword"); 
//
//		// login link opens login pop up
//		homePage.clickOnLoginLink();
//		homePage.verifyLoginPopUpPresence();
//		homePage.enterLoginForm(email, password);
//        homePage.verifyUserIsSignedIn();
//        homePage.clickOnSignOutLink();
//        homePage.verifyUserIsNotSignedIn();
//		  String y = "which psql";
//	      String runPsql = "psql -U ashishmantri --host=172.40.20.210 -d ashishmantri -c \"select id from profiles_user where email='treebotest@gmail.com';\"";
//
//	      String x = OsUtils.executeInlineShellScript(runPsql);
//	     
//	      System.out.println(x);
		//homePage.doSearch();	
		//Assert.assertTrue(false);
	}
	
//	@DataProvider(name = "dp")
//	public Object[][] createData(){
//        List<Integer> list = new ArrayList<Integer>();  
//        for (int i=0; i<5; i++) {  
//             list.add(i);  
//        }  
//        Integer[][] ret = new Integer[list.size()][];  
//        for (int i=0; i<list.size(); i++) {  
//             ret[i] = new Integer[]{list.get(i)};  
//        }  
//        return ret;  
//   } 
//	
	
//	@AfterMethod(alwaysRun = true)
//	public void cancelBooking() {
//		homePage = new HomePage(driver);
//		browserHelper = new BrowserHelper(driver);
//		bookingHistory = new BookingHistory(driver);
//		CommonUtils utils = new CommonUtils();
//		browserHelper.openUrl(browserHelper.getCurrentUrl());
//
//		if (!homePage.isUserSignedIn()) {
//			System.out.println("User is not signed in! Doing sign-in to cancel booking...");
//			homePage.signInAsTreeboMember(utils.getProperty("TestBookingAccount"),
//					utils.getProperty("TestLoginPassword"));
//		} else {
//			System.out.println("User is already signed in!");
//		}
//		bookingHistory.cancelAllBookings();
//	}
}
