package desktopUITest;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;

import org.apache.http.client.ClientProtocolException;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.testng.annotations.Test;
import org.xml.sax.SAXException;

import base.HotelLogixAPI;
import utils.DBUtils;

public class CancelBookingHXTest {
	
	@Test(groups={"CancelBooking"})
	public void testCancelBookingThroughHXAPI() throws SQLException, ClientProtocolException, IOException, ParserConfigurationException, SAXException{
		DBUtils db = new DBUtils();
		HotelLogixAPI api = new HotelLogixAPI();
		
		DateTime dt = new DateTime();
		DateTimeFormatter dtf = DateTimeFormat.forPattern("yyyy-MM-dd");
		String date = dtf.print(dt.minusDays(1));
		
		Map<String,String> openBookingInfo = db.getOrderIdAndReservationId("",date);
		for (String key : openBookingInfo.keySet()){
//			System.out.println(key);
//			System.out.println(openBookingInfo.get(key));
			api.cancelBooking(key, openBookingInfo.get(key));
		}
	}
}