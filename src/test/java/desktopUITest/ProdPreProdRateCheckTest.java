package desktopUITest;

import java.io.IOException;
import java.text.ParseException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import base.BrowserHelper;
import base.DriverManager;
import base.TestBaseSetUp;
import desktopUIPageObjects.BookingHistory;
import desktopUIPageObjects.ConfirmationPage;
import desktopUIPageObjects.HomePage;
import desktopUIPageObjects.HotelDetailsPage;
import desktopUIPageObjects.HotelResultsPage;
import desktopUIPageObjects.ItineraryPage;
import org.testng.Assert;
import org.testng.asserts.SoftAssert;
import utils.CommonUtils;

public class ProdPreProdRateCheckTest extends TestBaseSetUp {

	private HomePage homePage;
	private HotelResultsPage hotelResultsPage;
	private ItineraryPage itineraryPage;
	private ConfirmationPage confirmationPage;
	private HotelDetailsPage hotelDetailsPage;
	private BookingHistory bookingHistory;
	private BrowserHelper browserHelper;


	/***
	 * This test checks that in Prod for every hotel Hotel Details page loads
	 * And if default room is available
	 * 
	 * @throws IOException
	 * @throws ClientProtocolException
	 * @throws JSONException
	 */
	@Test(groups = { "PriceRateTax" })
	public void testPriceAndTax() throws ClientProtocolException, IOException, JSONException {
		WebDriver driver = DriverManager.getInstance().getDriver();
		homePage = new HomePage();
		hotelResultsPage = new HotelResultsPage();
		hotelDetailsPage = new HotelDetailsPage();
		itineraryPage = new ItineraryPage();
		browserHelper = new BrowserHelper();
		CommonUtils utils = new CommonUtils();

		String location = "India";
		int checkInFromCurrentDate = 1;
		int checkOutFromCurrentDate = checkInFromCurrentDate + 2;
		int roomIndex = 1;
		SoftAssert verify = new SoftAssert();

		homePage.doSearch(location, checkInFromCurrentDate, checkOutFromCurrentDate, roomIndex);
		hotelResultsPage.verifyHotelResultsPagePresence();

		// Checkin day - today , checkout - 7 + day
		// Each hotel
		// rate in search page and tax break up
		// Rack rate in HD page + All selectable rooms rate and tax
		List<WebElement> ele = driver.findElements(By.cssSelector(".hotel__name a"));
		System.out.println("Number of hotels:" + ele.size());
		Map<String, String> hotelData = new HashMap<String, String>();
		for (WebElement el : ele) {
			String x = el.getAttribute("href");
			System.out.println(x);
			String y = x.split("/")[4];
			System.out.println(y);
			String[] z = y.split("-");
			int a = z.length;
			String hotelId = z[a - 1].substring(1);
			String hotelName = "";
			for (int i = 0; i < a - 1; i++) {
				hotelName = hotelName + " " + z[i];
			}
			hotelData.put(hotelName, hotelId);
		}
		System.out.println(hotelData.toString());

		String checkin = null, checkout = null;
		// System.out.println("******* date *******");
		DateTime dt = new DateTime();
		DateTimeFormatter dtf = DateTimeFormat.forPattern("yyyy-MM-dd");
		// check availability for next 30 days
		String[] roomType = { "Acacia","Oak","Maple","Mahogany" };

		for (int i = 0; i < 30; i = i + 7) {
			checkin = dtf.print(dt.plusDays(i));
			checkout = dtf.print(dt.plusDays(i + 7));

			for (String key : hotelData.keySet()) {
				String hotelId = hotelData.get(key);
				for (String room : roomType) {

					String ifRoomHotelAva = String.format(
							"http://preprod.treebohotels.com/rest/v1/pricing/availability?couponcode=&roomconfig=1&roomtype=%s&checkin=2016-05-25&checkout=2016-05-26&hotelid=%s",
							room, hotelId);
					browserHelper.openUrl(ifRoomHotelAva);
					String responseroom = driver.findElement(By.cssSelector("body")).getText();
					JSONObject jsonObjroom = new JSONObject(responseroom);
					if (jsonObjroom.getJSONObject("data").getString("status").equals("success")) {

						System.out.println("checking for hotel :" + key);
						String preprod = String.format(
								"http://preprod.treebohotels.com/rest/v1/pricing/availability?couponcode=&roomconfig=1&roomtype=%s&checkin=%s&checkout=%s&hotelid=%s",
								room, checkin, checkout, hotelId);
						browserHelper.openUrl(preprod);
						String response = driver.findElement(By.cssSelector("body")).getText();
						JSONObject jsonObj = new JSONObject(response);

						String prod = String.format(
								"http://treebohotels.com/rest/v1/pricing/availability?couponcode=&roomconfig=1&roomtype=%s&checkin=%s&checkout=%s&hotelid=%s",
								room, checkin, checkout, hotelId);
						browserHelper.openUrl(prod);
						String response1 = driver.findElement(By.cssSelector("body")).getText();
						JSONObject jsonObj1 = new JSONObject(response1);
						System.out.println("******" + jsonObj.getJSONObject("data").getString("status"));
						if (jsonObj.getJSONObject("data").getString("status").equals("success")) {
							System.out.println("Starting comparing results");
							String preprodsellRate = jsonObj.getJSONObject("data").getString("sellRate");
							String preprodprice = jsonObj.getJSONObject("data").getString("price");
							String preprodtax = jsonObj.getJSONObject("data").getString("tax");
							JSONArray preprodnights = jsonObj.getJSONObject("data").getJSONArray("nights");
							JSONArray preprodroomdetails = jsonObj.getJSONObject("data").getJSONArray("roomdetails");

							String prodsellRate = jsonObj1.getJSONObject("data").getString("sellRate");
							String prodprice = jsonObj1.getJSONObject("data").getString("price");
							String prodtax = jsonObj1.getJSONObject("data").getString("tax");
							JSONArray prodnights = jsonObj1.getJSONObject("data").getJSONArray("nights");
							JSONArray prodroomdetails = jsonObj1.getJSONObject("data").getJSONArray("roomdetails");

							Assert.assertEquals(prodsellRate, preprodsellRate,
									"Failed for " + room + " " + checkin + "  " + checkout + " " + key);
							Assert.assertEquals(prodprice, preprodprice,
									"Failed for " + room + " " + checkin + "  " + checkout + " " + key);
							Assert.assertEquals(prodtax, preprodtax,
									"Failed for " + room + " " + checkin + "  " + checkout + " " + key);
							Assert.assertEquals(prodnights.toString(), preprodnights.toString(),
									"Failed for " + room + " " + checkin + "  " + checkout + " " + key);
							verify.assertEquals(prodroomdetails.toString(), preprodroomdetails.toString(),
									"Failed for " + room + " " + checkin + "  " + checkout + " " + key);
							System.out.println("end for hotel and room" + key + " " + room);
						}
					}
				}
			}
		}
		verify.assertAll();
	}
}