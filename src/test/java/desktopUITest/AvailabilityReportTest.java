package desktopUITest;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.testng.annotations.Test;
import com.google.common.collect.Maps;
import base.HotelLogixAPI;
import utils.DBUtils;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.io.FileNotFoundException;
import java.io.IOException;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;
import java.sql.SQLException;

public class AvailabilityReportTest {

	/**
	 * This test checks the availability in HX and DB and compare
	 * 
	 * @throws InterruptedException
	 * @throws IOException
	 * @throws SQLException
	 * @throws SAXException
	 * @throws ParserConfigurationException
	 * @throws FileNotFoundException
	 */
	@Test(groups = { "AvailabilityHotel" })
	public void testAvailabilityComparisonHXAndDB()
			throws IOException, InterruptedException, SQLException, ParserConfigurationException, SAXException {

		HotelLogixAPI api = new HotelLogixAPI();
		DBUtils db = new DBUtils();
		int loopCnt = Integer.parseInt(System.getProperty("numberOfDays"));
		String hotelNameToGet = System.getProperty("hotelName");
		String[] strHotelArray = hotelNameToGet.split(",");
		String checkin = null, checkout = null;
		//System.out.println("******* date *******");
		DateTime dt = new DateTime();
		DateTimeFormatter dtf = DateTimeFormat.forPattern("yyyy-MM-dd");
		// check availability for next 15 days
		
        for (String  x : strHotelArray){
        	String hotelNameToGetAvailability = x.trim();
    		System.out.println(" |     Date     |   HX Availability  |  DB Availability  | Hotel Name and Room Type             ");
    		System.out.println(" __________________________________________________________________________________________");
    		
    		for (int i = 0; i < loopCnt; i++) {
    			checkin = dtf.print(dt.plusDays(i));
    			checkout = dtf.print(dt.plusDays(i + 1));

    			//System.out.println("############################ " + checkin + " #############################\n");

    			Map<String, Integer> hxAvailability = api.hxRoomAvailabilityByHotelName(hotelNameToGetAvailability, checkin, checkout);
    			Map<String, Integer> dbAvailaiblity = db.getRoomAvailabilityByHotel(hotelNameToGetAvailability, checkin);
    			Set<String> setHXKey = hxAvailability.keySet();
    			Set<String> setDBKey = dbAvailaiblity.keySet();
    			Set<String> setAllKey = new HashSet<String>(setHXKey);
    			setAllKey.addAll(setDBKey);
    			
    			String dbCount = null;
    			String hxCount = null;
    			for (String key : setAllKey) {
    				try{
    					int hxcount = hxAvailability.get(key);
    					hxCount = String.format("%02d", hxcount);
    				}catch (Exception e){
    					hxCount = "  ";
					}
    				try{
    					int dbcount = dbAvailaiblity.get(key);
        				dbCount = String.format("%02d", dbcount);
    				}catch (Exception e){
    					dbCount = "  ";
    				}
        				System.out.println(" | "+checkin+"   |          " +hxCount+"        |        "    +dbCount+ "         |     " +  key             );
    			}
    		}
    		System.out.println("\n\n");
        }
		System.out.println("TreeboAvailability");
	}
}