package mobileSiteUITest;

import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import base.BrowserHelper;
import base.TestBaseSetUp;
import mobileSiteUIPageObjects.ConfirmationScreen;
import mobileSiteUIPageObjects.HomeScreen;
import mobileSiteUIPageObjects.HotelDetailsScreen;
import mobileSiteUIPageObjects.HotelSearchResultsScreen;
import mobileSiteUIPageObjects.ItineraryScreen;
import utils.CommonUtils;

public class MSiteCouponTest extends TestBaseSetUp {
	private HomeScreen homeScreen;
	private HotelSearchResultsScreen hotelSearchResultScreen;
	private HotelDetailsScreen hotelDetailsScreen;
	private ItineraryScreen itineraryScreen;
	private ConfirmationScreen confirmationScreen;
	private BrowserHelper browserHelper;

	/**
	 * This test checks Coupon can be applied
	 */

	@Test(groups = { "MSite" , "MSiteCoupon" })
	public void testMobileCouponApplication() {
		homeScreen = new HomeScreen();
		hotelSearchResultScreen = new HotelSearchResultsScreen();
		hotelDetailsScreen = new HotelDetailsScreen();
		itineraryScreen = new ItineraryScreen();
		confirmationScreen = new ConfirmationScreen();
		browserHelper = new BrowserHelper();
		CommonUtils utils = new CommonUtils();
		SoftAssert verify = new SoftAssert();

		String location = utils.getProperty("city"); // "Bengaluru";
		String testHotel = utils.getProperty("DummyHotel"); // "Elmas";
		String testUrl = utils.getProperty("TestUrl");
		int hotelIndex = 0;
		int checkInFromCurrentDate = utils.getRandomNumber(60, 90);
		int checkOutFromCurrentDate = checkInFromCurrentDate + 1;
		String coupon = utils.getProperty("Coupon");
		String couponDiscountPercent = utils.getProperty("CouponDiscountPercent");

		// Check if home screen is displayed
		verify.assertTrue(homeScreen.isHomeScreenDisplayed(), "Home Screen is not displayed");
		// Do Search
		homeScreen.doSearch(location, checkInFromCurrentDate, checkOutFromCurrentDate);
		// check if search results displayed
		verify.assertTrue(hotelSearchResultScreen.isHotelSearchScreenDisplayed(), "search result screen not displayed");
		// Select a random hotel or test hotel
		if (browserHelper.getCurrentUrl().contains(testUrl)) {
			hotelIndex = hotelSearchResultScreen.getAllAvailableHotelNames().indexOf(testHotel);
		} else {
			hotelIndex = hotelSearchResultScreen.getRandomIndexOfAvailableHotel();
		}

		String hotelName = hotelSearchResultScreen.getHotelNameByIndex(hotelIndex);
		int hotelRoomRate = hotelSearchResultScreen.getRoomRateByIndex(hotelIndex);
		System.out.println("Hotel Name: " + hotelName + " Hotel room rate : " + hotelRoomRate);

		// Click quick book
		hotelSearchResultScreen.clickQuickBookByIndex(hotelIndex);
		// Check If Itinerary screen
		verify.assertTrue(itineraryScreen.isItineraryScreenDisplayed());
		double iniBasePrice = itineraryScreen.getBasePrice();
		double iniDiscount = itineraryScreen.getDiscount();

		verify.assertEquals((int) Math.round(iniDiscount), 0, "Before coupon applied discount should have been zero");

		itineraryScreen.applyCoupon(coupon);
		int expectedCouponDiscount = (int) Math.round((iniBasePrice * Integer.parseInt(couponDiscountPercent)) / 100);
		int displaeyedDiscount = (int) Math.round(itineraryScreen.getCouponAppliedValue());
		verify.assertTrue(Math.abs(displaeyedDiscount - expectedCouponDiscount) <= 1,
				"Expected discount was : " + expectedCouponDiscount + " but displayed as " + displaeyedDiscount);
		verify.assertEquals((int) Math.round(displaeyedDiscount), (int) Math.round(itineraryScreen.getDiscount()),
				"Mismatch in coupon amount displayed and displayed as discount");

		// check calculations with discount applied
		verify.assertTrue(
				((int) Math.round(itineraryScreen.getTotalCost())
						- ((int) Math.round(itineraryScreen.getBasePrice()) + (int) Math.round(itineraryScreen.getTax())
								- (int) Math.round(itineraryScreen.getDiscount())) <= 1),
				"Mismatch in total cost vs sum of base price, tax and -discount");

		// Now remove coupon
		itineraryScreen.removeCouponApplied();
		verify.assertEquals((int) Math.round(itineraryScreen.getDiscount()), 0,
				"After coupon removed discount should have been zero");

		// check calculations with discount applied
		verify.assertTrue(
				Math.abs((int) Math.round(itineraryScreen.getTotalCost())
						- ((int) Math.round(itineraryScreen.getBasePrice())
								+ (int) Math.round(itineraryScreen.getTax()))) <= 1,
				"Mismatch in total cost vs sum of base price, tax and -discount(zero)");
		verify.assertAll();
	}

	/**
	 * This test checks Coupon can be applied
	 */

	@Test(groups = { "MSite" , "MSiteCoupon" })
	public void testMobileCouponApplicationTwoRoomMultipleGuests() {
		homeScreen = new HomeScreen();
		hotelSearchResultScreen = new HotelSearchResultsScreen();
		hotelDetailsScreen = new HotelDetailsScreen();
		itineraryScreen = new ItineraryScreen();
		confirmationScreen = new ConfirmationScreen();
		browserHelper = new BrowserHelper();
		CommonUtils utils = new CommonUtils();
		SoftAssert verify = new SoftAssert();

		String location = utils.getProperty("city"); // "Bengaluru";
		String testHotel = utils.getProperty("DummyHotel"); // "Elmas";
		String testUrl = utils.getProperty("TestUrl");
		int[][] roomIndex = { { 1, 2, 1 }, { 2, 1, 2 } };
		int hotelIndex = 0;
		int checkInFromCurrentDate = utils.getRandomNumber(60, 90);
		int checkOutFromCurrentDate = checkInFromCurrentDate + 1;
		String coupon = utils.getProperty("Coupon");
		String couponDiscountPercent = utils.getProperty("CouponDiscountPercent");

		// Check if home screen is displayed
		verify.assertTrue(homeScreen.isHomeScreenDisplayed(), "Home Screen is not displayed");
		// Do Search
		homeScreen.doSearch(location, checkInFromCurrentDate, checkOutFromCurrentDate, roomIndex);
		// check if search results displayed
		verify.assertTrue(hotelSearchResultScreen.isHotelSearchScreenDisplayed(), "search result screen not displayed");
		// Select a random hotel or test hotel
		if (browserHelper.getCurrentUrl().contains(testUrl)) {
			hotelIndex = hotelSearchResultScreen.getAllAvailableHotelNames().indexOf(testHotel);
		} else {
			hotelIndex = hotelSearchResultScreen.getRandomIndexOfAvailableHotel();
		}

		String hotelName = hotelSearchResultScreen.getHotelNameByIndex(hotelIndex);
		int hotelRoomRate = hotelSearchResultScreen.getRoomRateByIndex(hotelIndex);
		System.out.println("Hotel Name: " + hotelName + " Hotel room rate : " + hotelRoomRate);

		// Click quick book
		hotelSearchResultScreen.clickQuickBookByIndex(hotelIndex);
		// Check If Itinerary screen
		verify.assertTrue(itineraryScreen.isItineraryScreenDisplayed());
		double iniBasePrice = itineraryScreen.getBasePrice();
		double iniDiscount = itineraryScreen.getDiscount();

		verify.assertEquals((int) Math.round(iniDiscount), 0, "Before coupon applied discount should have been zero");

		itineraryScreen.applyCoupon(coupon);
		int expectedCouponDiscount = (int) Math.round((iniBasePrice * Integer.parseInt(couponDiscountPercent)) / 100);
		int displaeyedDiscount = (int) Math.round(itineraryScreen.getCouponAppliedValue());
		verify.assertTrue(Math.abs(displaeyedDiscount - expectedCouponDiscount) <= 1,
				"Expected discount was : " + expectedCouponDiscount + " but displayed as " + displaeyedDiscount);
		verify.assertEquals((int) Math.round(displaeyedDiscount), (int) Math.round(itineraryScreen.getDiscount()),
				"Mismatch in coupon amount displayed and displayed as discount");

		// check calculations with discount applied
		verify.assertTrue(
				((int) Math.round(itineraryScreen.getTotalCost())
						- ((int) Math.round(itineraryScreen.getBasePrice()) + (int) Math.round(itineraryScreen.getTax())
								- (int) Math.round(itineraryScreen.getDiscount())) <= 1),
				"Mismatch in total cost vs sum of base price, tax and -discount");

		// Now remove coupon
		itineraryScreen.removeCouponApplied();
		verify.assertEquals((int) Math.round(itineraryScreen.getDiscount()), 0,
				"After coupon removed discount should have been zero");

		// check calculations with discount applied
		verify.assertTrue(
				Math.abs((int) Math.round(itineraryScreen.getTotalCost())
						- ((int) Math.round(itineraryScreen.getBasePrice())
								+ (int) Math.round(itineraryScreen.getTax()))) <= 1,
				"Mismatch in total cost vs sum of base price, tax and -discount(zero)");
		verify.assertAll();
	}

	/**
	 * This test checks Invalid coupon error
	 */

	@Test(groups = { "MSite" , "MSiteCoupon" })
	public void testMobileInvalidCouponError() {
		homeScreen = new HomeScreen();
		hotelSearchResultScreen = new HotelSearchResultsScreen();
		hotelDetailsScreen = new HotelDetailsScreen();
		itineraryScreen = new ItineraryScreen();
		confirmationScreen = new ConfirmationScreen();
		browserHelper = new BrowserHelper();
		CommonUtils utils = new CommonUtils();
		SoftAssert verify = new SoftAssert();

		String location = utils.getProperty("city"); // "Bengaluru";
		String testHotel = utils.getProperty("DummyHotel"); // "Elmas";
		String testUrl = utils.getProperty("TestUrl");
		int hotelIndex = 0;
		int checkInFromCurrentDate = utils.getRandomNumber(60, 90);
		int checkOutFromCurrentDate = checkInFromCurrentDate + 1;
		String invalidCoupon = utils.getProperty("InvalidCoupon");

		// Check if home screen is displayed
		verify.assertTrue(homeScreen.isHomeScreenDisplayed(), "Home Screen is not displayed");
		// Do Search
		homeScreen.doSearch(location, checkInFromCurrentDate, checkOutFromCurrentDate);
		// check if search results displayed
		verify.assertTrue(hotelSearchResultScreen.isHotelSearchScreenDisplayed(), "search result screen not displayed");
		// Select a random hotel or test hotel
		if (browserHelper.getCurrentUrl().contains(testUrl)) {
			hotelIndex = hotelSearchResultScreen.getAllAvailableHotelNames().indexOf(testHotel);
		} else {
			hotelIndex = hotelSearchResultScreen.getRandomIndexOfAvailableHotel();
		}

		String hotelName = hotelSearchResultScreen.getHotelNameByIndex(hotelIndex);
		int hotelRoomRate = hotelSearchResultScreen.getRoomRateByIndex(hotelIndex);
		System.out.println("Hotel Name: " + hotelName + " Hotel room rate : " + hotelRoomRate);

		// Click quick book
		hotelSearchResultScreen.clickQuickBookByIndex(hotelIndex);
		// Check If Itinerary screen
		verify.assertTrue(itineraryScreen.isItineraryScreenDisplayed());
		double iniDiscount = itineraryScreen.getDiscount();

		verify.assertEquals((int) Math.round(iniDiscount), 0, "Before coupon applied discount should have been zero");

		itineraryScreen.applyCoupon(invalidCoupon);
		verify.assertTrue(itineraryScreen.invalidCouponErrorDisplayed(), "Invalid coupon error not displayed");
		verify.assertTrue(itineraryScreen.invalidCouponErrorIsAsExpected(), "Invalid coupon text does not match");
		verify.assertAll();
	}
	
	/**
	 * This test first apply invalid coupon and then apply correct coupon
	 */

	@Test(groups = { "MSite" , "MSiteCoupon" })
	public void testMobileCorrectCoupon() {
		homeScreen = new HomeScreen();
		hotelSearchResultScreen = new HotelSearchResultsScreen();
		hotelDetailsScreen = new HotelDetailsScreen();
		itineraryScreen = new ItineraryScreen();
		confirmationScreen = new ConfirmationScreen();
		browserHelper = new BrowserHelper();
		CommonUtils utils = new CommonUtils();
		SoftAssert verify = new SoftAssert();

		String location = utils.getProperty("city"); // "Bengaluru";
		String testHotel = utils.getProperty("DummyHotel"); // "Elmas";
		String testUrl = utils.getProperty("TestUrl");
		int hotelIndex = 0;
		int checkInFromCurrentDate = utils.getRandomNumber(60, 90);
		int checkOutFromCurrentDate = checkInFromCurrentDate + 1;
		String invalidCoupon = utils.getProperty("InvalidCoupon");
		String coupon = utils.getProperty("Coupon");
		String couponDiscountPercent = utils.getProperty("CouponDiscountPercent");

		// Check if home screen is displayed
		verify.assertTrue(homeScreen.isHomeScreenDisplayed(), "Home Screen is not displayed");
		// Do Search
		homeScreen.doSearch(location, checkInFromCurrentDate, checkOutFromCurrentDate);
		// check if search results displayed
		verify.assertTrue(hotelSearchResultScreen.isHotelSearchScreenDisplayed(), "search result screen not displayed");
		// Select a random hotel or test hotel
		if (browserHelper.getCurrentUrl().contains(testUrl)) {
			hotelIndex = hotelSearchResultScreen.getAllAvailableHotelNames().indexOf(testHotel);
		} else {
			hotelIndex = hotelSearchResultScreen.getRandomIndexOfAvailableHotel();
		}

		String hotelName = hotelSearchResultScreen.getHotelNameByIndex(hotelIndex);
		int hotelRoomRate = hotelSearchResultScreen.getRoomRateByIndex(hotelIndex);
		System.out.println("Hotel Name: " + hotelName + " Hotel room rate : " + hotelRoomRate);

		// Click quick book
		hotelSearchResultScreen.clickQuickBookByIndex(hotelIndex);
		// Check If Itinerary screen
		verify.assertTrue(itineraryScreen.isItineraryScreenDisplayed());
		double iniDiscount = itineraryScreen.getDiscount();

		verify.assertEquals((int) Math.round(iniDiscount), 0, "Before coupon applied discount should have been zero");

		itineraryScreen.applyCoupon(invalidCoupon);
		verify.assertTrue(itineraryScreen.invalidCouponErrorDisplayed(), "Invalid coupon error not displayed");
		verify.assertTrue(itineraryScreen.invalidCouponErrorIsAsExpected(), "Invalid coupon text does not match");
		
		// Now apply correct coupon
		double iniBasePrice = itineraryScreen.getBasePrice();
		itineraryScreen.applyCoupon(coupon);
		int expectedCouponDiscount = (int) Math.round((iniBasePrice * Integer.parseInt(couponDiscountPercent)) / 100);
		int displaeyedDiscount = (int) Math.round(itineraryScreen.getCouponAppliedValue());
		verify.assertTrue(Math.abs(displaeyedDiscount - expectedCouponDiscount) <= 1,
				"Expected discount was : " + expectedCouponDiscount + " but displayed as " + displaeyedDiscount);
		verify.assertEquals((int) Math.round(displaeyedDiscount), (int) Math.round(itineraryScreen.getDiscount()),
				"Mismatch in coupon amount displayed and displayed as discount");

		// check calculations with discount applied
		verify.assertTrue(
				((int) Math.round(itineraryScreen.getTotalCost())
						- ((int) Math.round(itineraryScreen.getBasePrice()) + (int) Math.round(itineraryScreen.getTax())
								- (int) Math.round(itineraryScreen.getDiscount())) <= 1),
				"Mismatch in total cost vs sum of base price, tax and -discount");

		// Now remove coupon
		itineraryScreen.removeCouponApplied();
		verify.assertEquals((int) Math.round(itineraryScreen.getDiscount()), 0,
				"After coupon removed discount should have been zero");

		// check calculations with discount applied
		verify.assertTrue(
				Math.abs((int) Math.round(itineraryScreen.getTotalCost())
						- ((int) Math.round(itineraryScreen.getBasePrice())
								+ (int) Math.round(itineraryScreen.getTax()))) <= 1,
				"Mismatch in total cost vs sum of base price, tax and -discount(zero)");
		verify.assertAll();
	}
}
