package pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

public class PaymentsScreen {
  private WebDriver driver;

  private By paymentsScreenTitle = By.xpath("//android.widget.TextView[@resource-id='com.treebo.bumblebee:id/toolbar_title'][@text='Payments']");
  private By recordPaymentButton = By.xpath("//android.widget.Button[@resource-id='com.treebo.bumblebee:id/button_record_payment'][@text='Record Payment']");
  private By payNow = By.xpath("//android.widget.RadioButton[@resource-id='com.treebo.bumblebee:id/radio_pay_now'][@text='Pay Now']");
  private By payLater = By.xpath("//android.widget.RadioButton[@resource-id='com.treebo.bumblebee:id/radio_pay_later'][@text='Pay Later']");
  private By cashPaymentButton = By.xpath("//android.widget.RadioButton[@resource-id='com.treebo.bumblebee:id/radio_cash'][@text='Cash Payment']");
  private By enterCashAmount = By.xpath("//android.widget.EditText[@resource-id='com.treebo.bumblebee:id/edit_pay_advance']");
  private By totalCashBalance = By.xpath("//android.widget.TextView[@resource-id='com.treebo.bumblebee:id/text_balance']"); // ₹ 1749.33
  private By checkOutButtonInPaymentsScreen = By.xpath("//android.widget.Button[@resource-id='com.treebo.bumblebee:id/button_checkout']");

  public PaymentsScreen(WebDriver driver) {
    this.driver=driver;
  }

  public void verifyPaymentsScreenPresence(){
    WebDriverWait wait = new WebDriverWait(driver, 120000);
    wait.until(ExpectedConditions.presenceOfElementLocated(paymentsScreenTitle));
    Assert.assertTrue(driver.findElement(paymentsScreenTitle).isDisplayed());
  }

  public void clickOnRecordPaymentButton(){
    WebDriverWait wait = new WebDriverWait(driver, 120000);
    wait.until(ExpectedConditions.presenceOfElementLocated(recordPaymentButton));
    driver.findElement(recordPaymentButton).click();
  }

  public void clickOnPayNowButton(){
    WebDriverWait wait = new WebDriverWait(driver, 120000);
    wait.until(ExpectedConditions.presenceOfElementLocated(payNow));
    driver.findElement(payNow).click();
  }

  public void clickOnPayLaterButton(){
    WebDriverWait wait = new WebDriverWait(driver, 120000);
    wait.until(ExpectedConditions.presenceOfElementLocated(payLater));
    driver.findElement(payLater).click();
  }

  public void clickOnCashPaymentButton(){
    WebDriverWait wait = new WebDriverWait(driver, 120000);
    wait.until(ExpectedConditions.presenceOfElementLocated(cashPaymentButton));
    driver.findElement(cashPaymentButton).click();
  }

  public String getTheTotalCashBalance(){
    WebDriverWait wait = new WebDriverWait(driver, 120000);
    wait.until(ExpectedConditions.presenceOfElementLocated(totalCashBalance));
    return driver.findElement(totalCashBalance).getText().split(" ")[1];
  }

  public void enterCashAmountToBePaid(String cashToBePaid){
    WebDriverWait wait = new WebDriverWait(driver, 120000);
    wait.until(ExpectedConditions.presenceOfElementLocated(enterCashAmount));
    driver.findElement(enterCashAmount).sendKeys(cashToBePaid);
  }

  public boolean getCheckOutButtonEnabledStatus(){
    return driver.findElement(checkOutButtonInPaymentsScreen).isEnabled();
  }

  public boolean getPayNowButtonEnabledStatus(){
    return driver.findElement(payNow).isEnabled();
  }

  public boolean getPayLaterButtonEnabledStatus(){
    return driver.findElement(payLater).isEnabled();
  }

  public void clickOnCheckOutButtonInPaymentsScreen(){
    driver.findElement(checkOutButtonInPaymentsScreen).click();
  }

}
