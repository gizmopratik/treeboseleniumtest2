package pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

public class BookingAmountSummaryScreen {
  private WebDriver driver;

  private By bookingAmountSummaryScreenTitle = By.xpath("//android.widget.TextView[@resource-id='com.treebo.bumblebee:id/toolbar_title'][@text='BOOKING ACCOUNT SUMMARY']");
  private By continueInBookingAmountSummary = By.xpath("//android.widget.Button[@resource-id='com.treebo.bumblebee:id/button_continue'][@text='Continue']");
  private By viewDetailsInBookingAmountSummary = By.xpath("//android.widget.Button[@resource-id='com.treebo.bumblebee:id/button_view_details']");

  private By bookingDetailsSection = By.xpath("//android.widget.TextView[@text='Booking Details']");
  private By guest_name = By.xpath("//android.widget.TextView[@resource-id='com.treebo.bumblebee:id/text_guest_name']");
  private By guest_email = By.xpath("//android.widget.TextView[@resource-id='com.treebo.bumblebee:id/text_guest_email']");
  private By guest_phone = By.xpath("//android.widget.TextView[@resource-id='com.treebo.bumblebee:id/text_guest_phone']");
  private By checkInCheckOut = By.xpath("//android.widget.TextView[@resource-id='com.treebo.bumblebee:id/text_checkin_checkout']");
  private By countOfGuest = By.xpath("//android.widget.TextView[@resource-id='com.treebo.bumblebee:id/text_guest_number']");
  private By numberOfRooms = By.xpath("//android.widget.TextView[@resource-id='com.treebo.bumblebee:id/text_number_rooms']");


  public BookingAmountSummaryScreen(WebDriver driver) {
    this.driver=driver;
  }

  public void verifyBookingAmountSummaryScreenPresence(){
    WebDriverWait wait = new WebDriverWait(driver, 120000);
    wait.until(ExpectedConditions.presenceOfElementLocated(bookingAmountSummaryScreenTitle));
    Assert.assertTrue(driver.findElement(bookingAmountSummaryScreenTitle).isDisplayed());
  }

  public void clickOnContinueInBookingAmountSummary(){
    driver.findElement(continueInBookingAmountSummary).click();
  }
  
  
}
