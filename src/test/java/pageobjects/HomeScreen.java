package pageobjects;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import utils.CommonUtils;

public class HomeScreen {
  private WebDriver driver;

  private By fdmHomeScreen = By.xpath("//android.widget.TextView[@resource-id='com.treebo.bumblebee:id/title'][@text='Bumblebee - FDM']");
  private By treeboLogo = By.xpath("//android.widget.ImageView[@resource-id='com.treebo.bumblebee:id/treebo_logo']");
  private By userSelectionDropDown = By.xpath("//android.widget.TextView[@text='Select user']");
  private By userNameList = By.xpath("//android.widget.CheckedTextView[@resource-id='android:id/text1']");
  private By continueButton = By.xpath("//android.widget.Button[@text='Continue']");
  private By fdmTitle = By.xpath("//android.widget.TextView[@resource-id='com.treebo.bumblebee:id/title_admin'][@text='Bumblebee - Admin']");
  private By loginButton = By.xpath("//android.widget.Button[@text='Login']");
  private By userErrorMessage = By.xpath("//android.widget.TextView[@resource-id='com.treebo.bumblebee:id/snackbar_text'][@text='Please select a user']");
  private By userErrorMessageCloseButton = By.xpath("//com.treebo.bumblebee:id/snackbar_action[@text='X']");
  private By withoutPinError = By.xpath("//android.widget.TextView[@resource-id='com.treebo.bumblebee:id/login_error_status'][@text='Please enter a PIN to continue']");
  private By incompletePinError = By.xpath("//android.widget.TextView[@resource-id='com.treebo.bumblebee:id/login_error_status'][@text='Pin length should be exactly 4 characters']");
  private By wrongPinErrorMessage = By.xpath("//android.widget.TextView[@resource-id='com.treebo.bumblebee:id/snackbar_text'][@text='Invalid Username or Password']");

  public HomeScreen(WebDriver driver) {
    this.driver=driver;
  }

  public void selectUser(String userName) throws FileNotFoundException, IOException{
    CommonUtils utils = new CommonUtils();
    String userXpath = String.format("//android.widget.CheckedTextView[@text='%s']",utils.getProperty(userName));
    driver.findElement(By.xpath(userXpath)).click();
  }

  public void enterPin(String[] pin){
    for (String s : pin ){
      String xpathPin = String.format("//android.widget.Button[@resource-id='com.treebo.bumblebee:id/passCodeInputButton'][@text='%s']",s);
      driver.findElement(By.xpath(xpathPin)).click();
    }
  }

  public void verifyHomeScreenPresence(){
    WebDriverWait wait = new WebDriverWait(driver, 120000);
    wait.until(ExpectedConditions.presenceOfElementLocated(fdmHomeScreen));
    Assert.assertTrue(driver.findElement(treeboLogo).isDisplayed());
    Assert.assertTrue(driver.findElement(fdmHomeScreen).isDisplayed());
    Assert.assertTrue(driver.findElement(fdmTitle).isDisplayed());
  }

  public void clickContinueButton(){
    driver.findElement(continueButton).click();
  }

  public void verifyUserErrorMessage(){
    Assert.assertTrue(driver.findElement(userErrorMessage).isDisplayed());
  }

  public void clickUserErrorMessageCloseButton(){
    driver.findElement(userErrorMessageCloseButton).click();
    Assert.assertTrue(!driver.findElement(userErrorMessage).isDisplayed());
  }

  public void signIn(String userName, String[] pin) throws FileNotFoundException, IOException{
    WebDriverWait wait = new WebDriverWait(driver, 60000);
    wait.until(ExpectedConditions.presenceOfElementLocated(userSelectionDropDown));
    driver.findElement(userSelectionDropDown).click();
    wait.until(ExpectedConditions.presenceOfElementLocated(userNameList));
    selectUser(userName);
    driver.findElement(continueButton).click();
    wait.until(ExpectedConditions.presenceOfElementLocated(loginButton));
    enterPin(pin);
    driver.findElement(loginButton).click();
  }

  public void selectUserAndContinue(String userName) throws FileNotFoundException, IOException{
    WebDriverWait wait = new WebDriverWait(driver, 60000);
    wait.until(ExpectedConditions.presenceOfElementLocated(userSelectionDropDown));
    driver.findElement(userSelectionDropDown).click();
    wait.until(ExpectedConditions.presenceOfElementLocated(userNameList));
    selectUser(userName);
    driver.findElement(continueButton).click();
  }

  public void clickLoginButton(){
    WebDriverWait wait = new WebDriverWait(driver, 60000);
    wait.until(ExpectedConditions.presenceOfElementLocated(loginButton));
    driver.findElement(loginButton).click();
  }

  public void verifyWithoutPinErrorMessage(){
    WebDriverWait wait = new WebDriverWait(driver, 60000);
    wait.until(ExpectedConditions.presenceOfElementLocated(withoutPinError));
    Assert.assertTrue(driver.findElement(withoutPinError).isDisplayed());
  }

  public void verifyIncompletePinErrorMessage(){
    WebDriverWait wait = new WebDriverWait(driver, 60000);
    wait.until(ExpectedConditions.presenceOfElementLocated(incompletePinError));
    Assert.assertTrue(!driver.findElement(incompletePinError).isDisplayed());
  }

  public void verifyWrongPinErrorMessage(){
    WebDriverWait wait = new WebDriverWait(driver, 60000);
    wait.until(ExpectedConditions.presenceOfElementLocated(wrongPinErrorMessage));
    Assert.assertTrue(!driver.findElement(wrongPinErrorMessage).isDisplayed());
  }
}
