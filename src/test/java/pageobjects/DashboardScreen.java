package pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import pageobjects.HomeScreen;

public class DashboardScreen {
  private WebDriver driver;

  private By dashboardWelcomeMessage = By.xpath("//android.widget.TextView[@text='Bumblebee - Guest']");
  private By navigationDrawer = By.xpath("//android.widget.ImageButton[@content-desc='Open navigation drawer']");
  private By logoutButton = By.xpath("//android.widget.TextView[@resource-id='com.treebo.bumblebee:id/logout']");
  private By logoutConfirmationPopUp = By.xpath("//android.widget.TextView[@resource-id='android:id/message'][@text='Are you sure you want to log out?']");
  private By confirmLogout = By.xpath("//android.widget.Button[@text='Yes']");
  private By cancelLogout = By.xpath("//android.widget.Button[@text='Cancel']");

  private By infoText = By.xpath("//android.widget.TextView[@text='Please select one of the options below to continue']");

  private By checkOutLink = By.xpath("//android.widget.TextView[@text='Check Out']");
  private By checkOutImage = By.xpath("//android.widget.TextView[@text='Check Out']/../android.widget.ImageView");

  private By checkInLink = By.xpath("//android.widget.TextView[@text='Check In']");
  private By checkInImage = By.xpath("//android.widget.TextView[@text='Check In']/../android.widget.ImageView");

  private By nightAuditLink = By.xpath("//android.widget.TextView[@text='Night Audit']");
  private By nightAuditImage = By.xpath("//android.widget.TextView[@text='Night Audit']/../android.widget.ImageView");

  private By houseViewLink = By.xpath("//android.widget.TextView[@text='House View']");
  private By houseViewImage = By.xpath("//android.widget.TextView[@text='House View']/../android.widget.ImageView");

  private By newEnquiryBookingLink = By.xpath("//android.widget.TextView[@text='New Enquiry/Booking']");
  private By newEnquiryBookingImage = By.xpath("//android.widget.TextView[@text='New Enquiry/Booking']/../android.widget.ImageView");



  public DashboardScreen(WebDriver driver) {
    this.driver=driver;
  }

  public void verifyDashboardScreenPresence(){
    WebDriverWait wait = new WebDriverWait(driver, 60000);
    wait.until(ExpectedConditions.presenceOfElementLocated(dashboardWelcomeMessage));
    Assert.assertTrue(driver.findElement(dashboardWelcomeMessage).isDisplayed());
  }

  public void logout(){
    WebDriverWait wait = new WebDriverWait(driver, 60000);
    HomeScreen home = new HomeScreen(driver);

    wait.until(ExpectedConditions.presenceOfElementLocated(navigationDrawer));
    driver.findElement(navigationDrawer).click();
    wait.until(ExpectedConditions.presenceOfElementLocated(logoutButton));
    driver.findElement(logoutButton).click();
    wait.until(ExpectedConditions.presenceOfElementLocated(logoutConfirmationPopUp));
    driver.findElement(confirmLogout).click();
    home.verifyHomeScreenPresence();
  }

  public void cancelLogout(){
    WebDriverWait wait = new WebDriverWait(driver, 60000);

    driver.findElement(navigationDrawer).click();
    wait.until(ExpectedConditions.presenceOfElementLocated(logoutButton));
    driver.findElement(logoutButton).click();
    wait.until(ExpectedConditions.presenceOfElementLocated(logoutConfirmationPopUp));
    driver.findElement(cancelLogout).click();
    Assert.assertTrue(!driver.findElement(logoutConfirmationPopUp).isDisplayed());
  }

  public void verifyDashboardUIElementsPresence(){
    verifyDashboardScreenPresence();
    Assert.assertTrue(driver.findElement(infoText).isDisplayed());
    Assert.assertTrue(driver.findElement(checkOutLink).isDisplayed());
    Assert.assertTrue(driver.findElement(checkOutImage).isDisplayed());
    Assert.assertTrue(driver.findElement(checkInLink).isDisplayed());
    Assert.assertTrue(driver.findElement(checkInImage).isDisplayed());
    Assert.assertTrue(driver.findElement(nightAuditLink).isDisplayed());
    Assert.assertTrue(driver.findElement(nightAuditImage).isDisplayed());
    Assert.assertTrue(driver.findElement(houseViewLink).isDisplayed());
    Assert.assertTrue(driver.findElement(houseViewImage).isDisplayed());
    Assert.assertTrue(driver.findElement(newEnquiryBookingLink).isDisplayed());
    Assert.assertTrue(driver.findElement(newEnquiryBookingImage).isDisplayed());
  }

  public void clickCheckOutLink(){
    WebDriverWait wait = new WebDriverWait(driver, 60000);
    wait.until(ExpectedConditions.presenceOfElementLocated(checkOutLink));
    driver.findElement(checkOutLink).click();
  }

}
