//package mobileUITest;
//
//import java.io.FileNotFoundException;
//import java.io.IOException;
//
//import org.openqa.selenium.WebDriver;
//import org.testng.annotations.BeforeClass;
//import org.testng.annotations.BeforeMethod;
//import org.testng.annotations.Test;
//
//import base.TestBaseSetUp;
//import pageobjects.HomeScreen;
//import pageobjects.DashboardScreen;
//
//
//public class LoginTest extends TestBaseSetUp {
//  private WebDriver driver;
//  private HomeScreen homeScreen;
//  private DashboardScreen dashboardScreen;
//
//  @BeforeMethod
//  public void setUp() {
//    driver = getDriver();
//  }
//
//  @Test(groups = {"HomeScreen", "Sanity"})
//  public void testSignInSuccessful() throws FileNotFoundException, IOException{
//    homeScreen = new HomeScreen(driver);
//    dashboardScreen = new DashboardScreen(driver);
//    String user = "userNameFDM";
//    String[] userPin = {"5","5","3","1"};
//
//    homeScreen.verifyHomeScreenPresence();
//    homeScreen.signIn(user,userPin);
//    dashboardScreen.verifyDashboardScreenPresence();
//    dashboardScreen.logout();
//  }
//
//  @Test(groups = {"HomeScreen", "High"})
//  public void testContinueWithoutUserSelection(){
//    homeScreen = new HomeScreen(driver);
//
//    homeScreen.verifyHomeScreenPresence();
//    homeScreen.clickContinueButton(); //Clicked without user selection
//    homeScreen.verifyUserErrorMessage();
//    homeScreen.clickUserErrorMessageCloseButton(); // Verifies that user error message is closed after clicking X
//  }
//
//  @Test(groups = {"HomeScreen", "High"})
//  public void testWithoutPinErrorMessage() throws FileNotFoundException, IOException{
//    homeScreen = new HomeScreen(driver);
//    String user = "userNameFDM";
//
//    homeScreen.verifyHomeScreenPresence();
//    homeScreen.selectUserAndContinue(user);
//    homeScreen.clickLoginButton();
//    homeScreen.verifyWithoutPinErrorMessage();
//  }
//
//  @Test(groups = {"HomeScreen", "High"})
//  public void testIncompletePinErrorMessage() throws FileNotFoundException, IOException{
//    homeScreen = new HomeScreen(driver);
//    String user = "userNameFDM";
//    String[] userPin = {"5","3"};
//
//    homeScreen.verifyHomeScreenPresence();
//    homeScreen.selectUserAndContinue(user);
//    homeScreen.enterPin(userPin);
//    homeScreen.clickLoginButton();
//    homeScreen.verifyIncompletePinErrorMessage();
//  }
//
//  @Test(groups = {"HomeScreen", "High"})
//  public void testWrongPinErrorMessage() throws FileNotFoundException, IOException{
//    homeScreen = new HomeScreen(driver);
//    String user = "userNameFDM";
//    String[] userPin = {"5","1","3","1"};
//
//    homeScreen.verifyHomeScreenPresence();
//    homeScreen.selectUserAndContinue(user);
//    homeScreen.enterPin(userPin);
//    homeScreen.clickLoginButton();
//    homeScreen.verifyIncompletePinErrorMessage();
//  }
//}
