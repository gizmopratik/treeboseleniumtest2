//package mobileUITest;
//
//import org.testng.Assert;
//import org.openqa.selenium.WebDriver;
//
//import java.io.FileNotFoundException;
//import java.io.IOException;
//
//import org.openqa.selenium.WebDriver;
//import org.testng.annotations.BeforeClass;
//import org.testng.annotations.BeforeMethod;
//import org.testng.annotations.Test;
//
//import base.TestBaseSetUp;
//import pageobjects.BookingAmountSummaryScreen;
//import pageobjects.DashboardScreen;
//import pageobjects.HomeScreen;
//import pageobjects.TodaysCheckoutScreen;
//import pageobjects.GuestsToCheckOutScreen;
//import pageobjects.PaymentsScreen;
//import pageobjects.GuestInvoiceScreen;
//
//public class CheckOutTest extends TestBaseSetUp{
//  private WebDriver driver;
//  private HomeScreen homeScreen;
//  private DashboardScreen dashboardScreen;
//  private TodaysCheckoutScreen todaysCheckoutScreen;
//  private BookingAmountSummaryScreen bookingAmountSummaryScreen;
//  private GuestsToCheckOutScreen guestsToCheckOutScreen;
//  private PaymentsScreen paymentsScreen;
//  private GuestInvoiceScreen guestInvoiceScreen;
//
//  @BeforeMethod
//  public void setUp() {
//    driver = getDriver();
//  }
//
//  @Test(groups = {"CheckOutFlow", "Sanity"})
//  public void testCheckOutLinkNavigateToTodaysCheckOut() throws FileNotFoundException, IOException{
//    homeScreen = new HomeScreen(driver);
//    dashboardScreen = new DashboardScreen(driver);
//    todaysCheckoutScreen = new TodaysCheckoutScreen(driver);
//
//    String user = "userNameFDM";
//    String[] userPin = {"5","5","3","1"};
//
//    homeScreen.verifyHomeScreenPresence();
//    homeScreen.signIn(user,userPin);
//    dashboardScreen.verifyDashboardScreenPresence();
//    dashboardScreen.clickCheckOutLink();
//    todaysCheckoutScreen.verifyTodaysCheckoutScreenPresence();
//  }
//
//  @Test(groups = {"CheckOutFlow", "High"})
//  public void testGuestDetailsOnTiles() throws FileNotFoundException, IOException{
//    homeScreen = new HomeScreen(driver);
//    dashboardScreen = new DashboardScreen(driver);
//    todaysCheckoutScreen = new TodaysCheckoutScreen(driver);
//
//    String user = "userNameFDM";
//    String[] userPin = {"5","5","3","1"};
//
//    homeScreen.verifyHomeScreenPresence();
//    homeScreen.signIn(user,userPin);
//    dashboardScreen.verifyDashboardScreenPresence();
//    dashboardScreen.clickCheckOutLink();
//    todaysCheckoutScreen.verifyTodaysCheckoutScreenPresence();
//    todaysCheckoutScreen.verifyGuestDetailsOnTiles();
//  }
//
//  @Test(groups = {"CheckOutFlow", "High"})
//  public void testRefreshAndNavigateBack() throws FileNotFoundException, IOException{
//    homeScreen = new HomeScreen(driver);
//    dashboardScreen = new DashboardScreen(driver);
//    todaysCheckoutScreen = new TodaysCheckoutScreen(driver);
//
//    String user = "userNameFDM";
//    String[] userPin = {"5","5","3","1"};
//
//    homeScreen.verifyHomeScreenPresence();
//    homeScreen.signIn(user,userPin);
//    dashboardScreen.verifyDashboardScreenPresence();
//    dashboardScreen.clickCheckOutLink();
//    todaysCheckoutScreen.verifyTodaysCheckoutScreenPresence();
//    todaysCheckoutScreen.clickOnRefreshButtonInTodaysCheckOutScreen();
//    todaysCheckoutScreen.verifyTodaysCheckoutScreenPresence();
//    todaysCheckoutScreen.clickOnNavigateBack();
//    dashboardScreen.verifyDashboardScreenPresence();
//  }
//
//  @Test(groups = {"CheckOutFlow", "High"})
//  public void testFutureCheckoutsAreDisabledAndCheckOutButtonIsDisabled() throws FileNotFoundException, IOException{
//    homeScreen = new HomeScreen(driver);
//    dashboardScreen = new DashboardScreen(driver);
//    todaysCheckoutScreen = new TodaysCheckoutScreen(driver);
//    bookingAmountSummaryScreen = new BookingAmountSummaryScreen(driver);
//
//    String user = "userNameFDM";
//    String[] userPin = {"5","5","3","1"};
//
//    homeScreen.verifyHomeScreenPresence();
//    homeScreen.signIn(user,userPin);
//    dashboardScreen.verifyDashboardScreenPresence();
//    dashboardScreen.clickCheckOutLink();
//    todaysCheckoutScreen.verifyTodaysCheckoutScreenPresence();
//    todaysCheckoutScreen.navigateToFutureCheckOutsWithBooking();
//    todaysCheckoutScreen.clickOnFirstCheckoutTile();
//    todaysCheckoutScreen.verifyCheckOutButtonStatusIsDisabled();
//  }
//
//  @Test(groups = {"CheckOutFlow", "High"})
//  public void testCheckoutSingleRoomBookingSuccessful() throws FileNotFoundException, IOException{
//    homeScreen = new HomeScreen(driver);
//    dashboardScreen = new DashboardScreen(driver);
//    todaysCheckoutScreen = new TodaysCheckoutScreen(driver);
//    guestsToCheckOutScreen = new GuestsToCheckOutScreen(driver);
//    paymentsScreen = new PaymentsScreen(driver);
//    bookingAmountSummaryScreen = new BookingAmountSummaryScreen(driver);
//    guestInvoiceScreen = new GuestInvoiceScreen(driver);
//
//
//    String user = "userNameFDM";
//    String[] userPin = {"5","5","3","1"};
//    String bookingName = "JUSTIN JIN  + 1";
//
//    homeScreen.verifyHomeScreenPresence();
//    homeScreen.signIn(user,userPin);
//    dashboardScreen.verifyDashboardScreenPresence();
//    dashboardScreen.clickCheckOutLink();
//    todaysCheckoutScreen.verifyTodaysCheckoutScreenPresence();
//    todaysCheckoutScreen.clickOnBookingTileByName(bookingName);
//    todaysCheckoutScreen.clickOnCheckoutButton();
//    bookingAmountSummaryScreen.verifyBookingAmountSummaryScreenPresence();
//    bookingAmountSummaryScreen.clickOnContinueInBookingAmountSummary();
//    guestsToCheckOutScreen.verifyGuestsToCheckOutScreenPresence();
//    guestsToCheckOutScreen.selectAllCheckboxForCheckOut();
//    guestsToCheckOutScreen.clickOnContinueButtonInGuestsToCheckoutScreen();
//    paymentsScreen.verifyPaymentsScreenPresence();
//    paymentsScreen.clickOnRecordPaymentButton();
//    paymentsScreen.clickOnPayNowButton();
//    paymentsScreen.clickOnCashPaymentButton();
//    Assert.assertTrue(!paymentsScreen.getCheckOutButtonEnabledStatus());
//    paymentsScreen.enterCashAmountToBePaid(paymentsScreen.getTheTotalCashBalance());
//    Assert.assertTrue(paymentsScreen.getCheckOutButtonEnabledStatus());
////    paymentsScreen.clickOnCheckOutButtonInPaymentsScreen();
////    guestInvoiceScreen.verifyGuestInvoiceScreenPresence();
//  }
//
//  @Test(groups = {"CheckOutFlow", "High"})
//  public void testCheckoutPrepaidBookingPaymentOptionsDisabled() throws FileNotFoundException, IOException{
//    homeScreen = new HomeScreen(driver);
//    dashboardScreen = new DashboardScreen(driver);
//    todaysCheckoutScreen = new TodaysCheckoutScreen(driver);
//    guestsToCheckOutScreen = new GuestsToCheckOutScreen(driver);
//    paymentsScreen = new PaymentsScreen(driver);
//    bookingAmountSummaryScreen = new BookingAmountSummaryScreen(driver);
//    guestInvoiceScreen = new GuestInvoiceScreen(driver);
//
//
//    String user = "userNameFDM";
//    String[] userPin = {"5","5","3","1"};
//    String bookingName = "Ravikanth Halanchal  + 1";
//
//    homeScreen.verifyHomeScreenPresence();
//    homeScreen.signIn(user,userPin);
//    dashboardScreen.verifyDashboardScreenPresence();
//    dashboardScreen.clickCheckOutLink();
//    todaysCheckoutScreen.verifyTodaysCheckoutScreenPresence();
//    todaysCheckoutScreen.clickOnBookingTileByName(bookingName);
//    todaysCheckoutScreen.clickOnCheckoutButton();
//    bookingAmountSummaryScreen.verifyBookingAmountSummaryScreenPresence();
//    bookingAmountSummaryScreen.clickOnContinueInBookingAmountSummary();
//    guestsToCheckOutScreen.verifyGuestsToCheckOutScreenPresence();
//    guestsToCheckOutScreen.selectAllCheckboxForCheckOut();
//    guestsToCheckOutScreen.clickOnContinueButtonInGuestsToCheckoutScreen();
//    paymentsScreen.verifyPaymentsScreenPresence();
//    paymentsScreen.clickOnRecordPaymentButton();
//    Assert.assertTrue(!paymentsScreen.getPayNowButtonEnabledStatus());
//    Assert.assertTrue(!paymentsScreen.getPayLaterButtonEnabledStatus());
//    Assert.assertTrue(paymentsScreen.getCheckOutButtonEnabledStatus());
////    paymentsScreen.clickOnCheckOutButtonInPaymentsScreen();
////    guestInvoiceScreen.verifyGuestInvoiceScreenPresence();
//  }
//
//  @Test(groups = {"CheckOutFlow", "High"})
//  public void testCheckoutBTCBookingPaymentOptionsDisabledNoInvoice() throws FileNotFoundException, IOException{
//    homeScreen = new HomeScreen(driver);
//    dashboardScreen = new DashboardScreen(driver);
//    todaysCheckoutScreen = new TodaysCheckoutScreen(driver);
//    guestsToCheckOutScreen = new GuestsToCheckOutScreen(driver);
//    paymentsScreen = new PaymentsScreen(driver);
//    bookingAmountSummaryScreen = new BookingAmountSummaryScreen(driver);
//    guestInvoiceScreen = new GuestInvoiceScreen(driver);
//
//
//    String user = "userNameFDM";
//    String[] userPin = {"5","5","3","1"};
//    String bookingName = "Treebo Hotel  + 2";
//
//    homeScreen.verifyHomeScreenPresence();
//    homeScreen.signIn(user,userPin);
//    dashboardScreen.verifyDashboardScreenPresence();
//    dashboardScreen.clickCheckOutLink();
//    todaysCheckoutScreen.verifyTodaysCheckoutScreenPresence();
//    todaysCheckoutScreen.clickOnBookingTileByName(bookingName);
//    todaysCheckoutScreen.clickOnCheckoutButton();
//    bookingAmountSummaryScreen.verifyBookingAmountSummaryScreenPresence();
//    bookingAmountSummaryScreen.clickOnContinueInBookingAmountSummary();
//    guestsToCheckOutScreen.verifyGuestsToCheckOutScreenPresence();
//    guestsToCheckOutScreen.selectAllCheckboxForCheckOut();
//    guestsToCheckOutScreen.clickOnContinueButtonInGuestsToCheckoutScreen();
//    paymentsScreen.verifyPaymentsScreenPresence();
//    paymentsScreen.clickOnRecordPaymentButton();
//    Assert.assertTrue(!paymentsScreen.getPayNowButtonEnabledStatus());
//    Assert.assertTrue(!paymentsScreen.getPayLaterButtonEnabledStatus());
//    Assert.assertTrue(paymentsScreen.getCheckOutButtonEnabledStatus());
////    paymentsScreen.clickOnCheckOutButtonInPaymentsScreen();
////   todaysCheckoutScreen.verifyTodaysCheckoutScreenPresence();
//  }
//
//  @Test(groups = {"CheckOutFlow", "High"})
//  public void testCheckoutAllGroupBookingSuccessfulPayNow() throws FileNotFoundException, IOException{
//    homeScreen = new HomeScreen(driver);
//    dashboardScreen = new DashboardScreen(driver);
//    todaysCheckoutScreen = new TodaysCheckoutScreen(driver);
//    guestsToCheckOutScreen = new GuestsToCheckOutScreen(driver);
//    paymentsScreen = new PaymentsScreen(driver);
//    bookingAmountSummaryScreen = new BookingAmountSummaryScreen(driver);
//    guestInvoiceScreen = new GuestInvoiceScreen(driver);
//
//
//    String user = "userNameFDM";
//    String[] userPin = {"5","5","3","1"};
//    String bookingName = "Nithya reddy  + 3";
//
//    homeScreen.verifyHomeScreenPresence();
//    homeScreen.signIn(user,userPin);
//    dashboardScreen.verifyDashboardScreenPresence();
//    dashboardScreen.clickCheckOutLink();
//    todaysCheckoutScreen.verifyTodaysCheckoutScreenPresence();
//    todaysCheckoutScreen.clickOnBookingTileByName(bookingName);
//    todaysCheckoutScreen.clickOnCheckoutButton();
//    bookingAmountSummaryScreen.verifyBookingAmountSummaryScreenPresence();
//    bookingAmountSummaryScreen.clickOnContinueInBookingAmountSummary();
//    guestsToCheckOutScreen.verifyGuestsToCheckOutScreenPresence();
//    guestsToCheckOutScreen.selectAllCheckboxForCheckOut();
//    guestsToCheckOutScreen.clickOnContinueButtonInGuestsToCheckoutScreen();
//    paymentsScreen.verifyPaymentsScreenPresence();
//    paymentsScreen.clickOnRecordPaymentButton();
//    paymentsScreen.clickOnPayNowButton();
//    paymentsScreen.clickOnCashPaymentButton();
//    Assert.assertTrue(!paymentsScreen.getCheckOutButtonEnabledStatus());
//    paymentsScreen.enterCashAmountToBePaid(paymentsScreen.getTheTotalCashBalance());
//    Assert.assertTrue(paymentsScreen.getCheckOutButtonEnabledStatus());
////    paymentsScreen.clickOnCheckOutButtonInPaymentsScreen();
////    guestInvoiceScreen.verifyGuestInvoiceScreenPresence();
//  }
//
//  @Test(groups = {"CheckOutFlow", "High"})
//  public void testCheckoutPartialGroupBookingSuccessfulPayNow() throws FileNotFoundException, IOException{
//    homeScreen = new HomeScreen(driver);
//    dashboardScreen = new DashboardScreen(driver);
//    todaysCheckoutScreen = new TodaysCheckoutScreen(driver);
//    guestsToCheckOutScreen = new GuestsToCheckOutScreen(driver);
//    paymentsScreen = new PaymentsScreen(driver);
//    bookingAmountSummaryScreen = new BookingAmountSummaryScreen(driver);
//    guestInvoiceScreen = new GuestInvoiceScreen(driver);
//
//
//    String user = "userNameFDM";
//    String[] userPin = {"5","5","3","1"};
//    String bookingName = "jimmy chan  + 3";
//    String[][] roomAndGuestsToCheckOut ={{"1","1","2"}}; //first room both guests
//
//    homeScreen.verifyHomeScreenPresence();
//    homeScreen.signIn(user,userPin);
//    dashboardScreen.verifyDashboardScreenPresence();
//    dashboardScreen.clickCheckOutLink();
//    todaysCheckoutScreen.verifyTodaysCheckoutScreenPresence();
//    todaysCheckoutScreen.clickOnBookingTileByName(bookingName);
//    todaysCheckoutScreen.clickOnCheckoutButton();
//    bookingAmountSummaryScreen.verifyBookingAmountSummaryScreenPresence();
//    bookingAmountSummaryScreen.clickOnContinueInBookingAmountSummary();
//    guestsToCheckOutScreen.verifyGuestsToCheckOutScreenPresence();
//    guestsToCheckOutScreen.selectGuestsByRoomAndGuestNumber(roomAndGuestsToCheckOut);
//    guestsToCheckOutScreen.clickOnContinueButtonInGuestsToCheckoutScreen();
//    paymentsScreen.verifyPaymentsScreenPresence();
//    paymentsScreen.clickOnRecordPaymentButton();
//    paymentsScreen.clickOnPayNowButton();
//    paymentsScreen.clickOnCashPaymentButton();
//    Assert.assertTrue(!paymentsScreen.getCheckOutButtonEnabledStatus());
//    paymentsScreen.enterCashAmountToBePaid(paymentsScreen.getTheTotalCashBalance());
//    Assert.assertTrue(paymentsScreen.getCheckOutButtonEnabledStatus());
////    paymentsScreen.clickOnCheckOutButtonInPaymentsScreen();
////    guestInvoiceScreen.verifyGuestInvoiceScreenPresence();
//  }
//
//  @Test(groups = {"CheckOutFlow", "High"})
//  public void testCheckoutPartialGroupBookingSuccessfulPayLater() throws FileNotFoundException, IOException{
//    homeScreen = new HomeScreen(driver);
//    dashboardScreen = new DashboardScreen(driver);
//    todaysCheckoutScreen = new TodaysCheckoutScreen(driver);
//    guestsToCheckOutScreen = new GuestsToCheckOutScreen(driver);
//    paymentsScreen = new PaymentsScreen(driver);
//    bookingAmountSummaryScreen = new BookingAmountSummaryScreen(driver);
//    guestInvoiceScreen = new GuestInvoiceScreen(driver);
//
//
//    String user = "userNameFDM";
//    String[] userPin = {"5","5","3","1"};
//    String bookingName = "Tejes Naidu  + 4";
//    String[][] roomAndGuestsToCheckOut ={{"1","1"}}; //first room first guest
//
//    homeScreen.verifyHomeScreenPresence();
//    homeScreen.signIn(user,userPin);
//    dashboardScreen.verifyDashboardScreenPresence();
//    dashboardScreen.clickCheckOutLink();
//    todaysCheckoutScreen.verifyTodaysCheckoutScreenPresence();
//    todaysCheckoutScreen.clickOnBookingTileByName(bookingName);
//    todaysCheckoutScreen.clickOnCheckoutButton();
//    bookingAmountSummaryScreen.verifyBookingAmountSummaryScreenPresence();
//    bookingAmountSummaryScreen.clickOnContinueInBookingAmountSummary();
//    guestsToCheckOutScreen.verifyGuestsToCheckOutScreenPresence();
//    guestsToCheckOutScreen.selectGuestsByRoomAndGuestNumber(roomAndGuestsToCheckOut);
//    guestsToCheckOutScreen.clickOnContinueButtonInGuestsToCheckoutScreen();
//    paymentsScreen.verifyPaymentsScreenPresence();
//    paymentsScreen.clickOnRecordPaymentButton();
//    Assert.assertTrue(!paymentsScreen.getCheckOutButtonEnabledStatus());
//    paymentsScreen.clickOnPayLaterButton();
//    Assert.assertTrue(paymentsScreen.getCheckOutButtonEnabledStatus());
////    paymentsScreen.clickOnCheckOutButtonInPaymentsScreen();
////    todaysCheckoutScreen.verifyTodaysCheckoutScreenPresence();
//  }
//
//}
