package mobileSiteUIPageObjects;

import org.apache.commons.lang3.RandomStringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import base.BrowserHelper;
import base.DriverManager;
import mobileSiteUIPageObjects.HomeScreen;
import utils.CommonUtils;

public class ItineraryScreen {
	private BrowserHelper browserHelper;
	private HomeScreen homeScreen;

	public boolean isItineraryScreenDisplayed() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		WebDriverWait wait = new WebDriverWait(driver, 120);
		wait.until(ExpectedConditions
				.presenceOfElementLocated(By.cssSelector(".itinerary-page__review__heading .heading")));
		return driver.findElement(By.cssSelector(".itinerary-page__review__heading .heading")).isDisplayed();
	}

	public double getTotalCost() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		return Double.parseDouble(driver.findElement(By.cssSelector("#grandTotal")).getText().trim());
	}

	public double getBasePrice() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		return Double.parseDouble(driver.findElement(By.cssSelector(".pretax-price")).getText().trim());
	}

	public double getTax() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		return Double.parseDouble(driver.findElement(By.cssSelector("#totalTax")).getText().trim());
	}

	public double getDiscount() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		return Double.parseDouble(driver.findElement(By.cssSelector("#discountValue")).getText().trim());
	}

	public void applyCoupon(String couponName) {
		WebDriver driver = DriverManager.getInstance().getDriver();
		browserHelper = new BrowserHelper();
		driver.findElement(By.cssSelector(".analytics-voucher")).sendKeys(couponName);
		driver.findElement(By.cssSelector(".discount__applybtn")).click();
		browserHelper.waitTime(1000);
		WebDriverWait wait = new WebDriverWait(driver, 120);
		try {
			wait.until(ExpectedConditions
					.presenceOfElementLocated(By.cssSelector("#discountCouponContainer .loader-container.hide")));
		} catch (Exception e) {
			// Do nothing
		}
	}

	public double getCouponAppliedValue() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		return Double.parseDouble(driver.findElement(By.cssSelector(".analytics-discountvalue")).getText().trim());
	}

	public void removeCouponApplied() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		browserHelper = new BrowserHelper();
		driver.findElement(By.xpath("//a[text()='remove']")).click();
		browserHelper.waitTime(3000);
	}

	public boolean invalidCouponErrorDisplayed() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		return driver.findElement(By.cssSelector("#discountError")).isDisplayed();
	}

	public boolean invalidCouponErrorIsAsExpected() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		System.out.println(
				"Invalid coupon error text : " + driver.findElement(By.cssSelector("#discountError")).getText());
		return (driver.findElement(By.cssSelector("#discountError")).getText()
				.equals("Sorry! This coupon code is not valid"));
	}

	public String getHotelName() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		return driver.findElement(By.cssSelector(".itinerary-page__review__content__hotel__info h3")).getText();
	}

	public String getItineraryCheckInDate() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		return driver.findElement(By.cssSelector(".analytics-checkin")).getText();
	}

	public String getItineraryCheckOutDate() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		return driver.findElement(By.cssSelector(".analytics-checkout")).getText();
	}

	public String getGuestName() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		return driver.findElement(By.cssSelector("#guestName")).getAttribute("value");
	}

	public String getGuestMobile() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		return driver.findElement(By.cssSelector("#guestMobile")).getAttribute("value");
	}

	public String getGuestEmail() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		return driver.findElement(By.cssSelector("#guestEmail")).getAttribute("value");
	}

	// public String getItinerayRoomType(){
	// WebDriver driver = DriverManager.getInstance().getDriver();
	// return
	// driver.findElement(By.cssSelector(".itinerary-page__review__content__hotel__info
	// h3")).getText();
	// }
	//
	// public int getItinerayDuration(){
	// WebDriver driver = DriverManager.getInstance().getDriver();
	// return
	// driver.findElement(By.cssSelector(".itinerary-page__review__content__hotel__info
	// h3")).getText();
	// }
	//
	// public int getItinerayNumOfGuests(){
	// WebDriver driver = DriverManager.getInstance().getDriver();
	// return
	// driver.findElement(By.cssSelector(".itinerary-page__review__content__hotel__info
	// h3")).getText();
	// }
	//
	// public int getItinerayNumOfRooms(){
	// WebDriver driver = DriverManager.getInstance().getDriver();
	// return
	// driver.findElement(By.cssSelector(".itinerary-page__review__content__hotel__info
	// h3")).getText();
	// }

	public void clickOnNextStepOne() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		driver.findElement(By.cssSelector(".analytics-next")).click();
	}

	public void clickOnNextOnStepTwo() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		driver.findElement(By.cssSelector(".itinerary-page__action__button")).click();
	}

	public void loginAsTreeboMember(String strEmail, String strPassword) {
		WebDriver driver = DriverManager.getInstance().getDriver();
		browserHelper = new BrowserHelper();
		driver.findElement(By.cssSelector("#loginEmailInput")).sendKeys(strEmail);
		driver.findElement(By.cssSelector("#loginPassowrdlInput")).sendKeys(strPassword);
		driver.findElement(By.cssSelector("#loginButton")).click();
		browserHelper.waitTime(3000);
	}

	public void continueAsGuest() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		browserHelper = new BrowserHelper();
		driver.findElement(By.cssSelector("#countinueGuest")).click();
		browserHelper.waitTime(3000);
	}

	public void enterGuestDetails(String strName, String strMobile, String strEmail) {
		WebDriver driver = DriverManager.getInstance().getDriver();
		driver.findElement(By.cssSelector("#guestName")).sendKeys(strName);
		driver.findElement(By.cssSelector("#guestMobile")).sendKeys(strMobile);
		driver.findElement(By.cssSelector("#guestEmail")).sendKeys(strEmail);
	}

	public void clickOnRegister() {
		browserHelper = new BrowserHelper();
		WebDriver driver = DriverManager.getInstance().getDriver();
		driver.findElement(By.cssSelector(".login__register")).click();
		browserHelper.waitTime(3000);
	}

	public void signUpOnItineraryScreen(String strName, String strMobile, String strEmail, String strPassword) {
		WebDriver driver = DriverManager.getInstance().getDriver();
		browserHelper = new BrowserHelper();
		homeScreen = new HomeScreen();
		driver.findElement(By.cssSelector("#signupNamelInput")).sendKeys(strName);
		driver.findElement(By.cssSelector("#signupMobileInput")).sendKeys(strMobile);
		driver.findElement(By.cssSelector("#signupEmailInput")).sendKeys(strEmail);
		driver.findElement(By.cssSelector("#signupPassowrdlInput")).sendKeys(strPassword);
		driver.findElement(By.cssSelector("#signupButton")).click();
		homeScreen.waitForLoaderToDisappear();
		browserHelper.waitTime(3000);
	}

	public String[] registerOnItineraryScreen() {
		CommonUtils utils = new CommonUtils();
		String name = "Test";
		String uniqueEmail = "test" + RandomStringUtils.randomAlphanumeric(15);
		String email = uniqueEmail + "@gmail.com";
		String mobile = Integer.toString(utils.getRandomNumber(2, 6)) + RandomStringUtils.randomNumeric(9);
		String password = "password";
		String[] regInfo = { name, email, mobile, password };
		signUpOnItineraryScreen(name, mobile, email, password);

		System.out.println("Registered with name : " + name + " email : " + email + " mobile : " + mobile
				+ " password : " + password);
		return regInfo;
	}

	public void clickOnPayAtHotel() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		driver.findElement(By.cssSelector("#payatHotel")).click();
	}

	public void clickOnPayNow() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		driver.findElement(By.cssSelector("#payNow")).click();
	}

	public boolean isBackButtonThere() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		return (driver.findElements(By.cssSelector(".back.js-back-link.sub-heading.itinerary-steps__heading"))
				.size() > 0);
	}

	public void clickOnBackOnItineraryScreen() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		driver.findElement(By.cssSelector(".back.js-back-link.sub-heading.itinerary-steps__heading")).click();
	}

	public boolean isRazorPayScreenDisplayed() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		return driver.findElement(By.cssSelector(".merchant-image")).isDisplayed();
	}

	public void closeRazorPayScreen() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		driver.findElement(By.cssSelector("#modal-close")).click();
	}

}
