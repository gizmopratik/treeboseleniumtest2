package mobileSiteUIPageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import base.DriverManager;

public class ConfirmationScreen {

	public boolean isConfirmationScreenDisplayed(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		WebDriverWait wait = new WebDriverWait(driver, 120);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[text()='Your booking is confirmed']")));
		return driver.findElement(By.xpath("//div[text()='Your booking is confirmed']")).isDisplayed();
	}
	
	public String getBookingId(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		return driver.findElement(By.xpath("//div[@class='bookingInfo__item'][1]/div[2]")).getText();
	}
	
	public String getGuestName(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		return driver.findElement(By.xpath("//div[@class='bookingInfo__item'][2]/div[2]")).getText();
	}
	
	public String getHotelName(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		return driver.findElement(By.cssSelector(".hotelDetail__hotel-name")).getText();
	}
	
	public String getCheckInDate(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		return driver.findElement(By.xpath("//div[@class='bookingInfo__item'][3]/div[2]")).getText();
	}
	
	public String getCheckOutDate(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		return driver.findElement(By.xpath("//div[@class='bookingInfo__item'][4]/div[2]")).getText();
	}
	
	public int getRoomCount(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		return Integer.parseInt(driver.findElement(By.xpath("//div[@class='bookingInfo__item'][5]/div[2]")).getText().split(" ")[0]);
	}
	
	public int getGuestCount(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		return Integer.parseInt(driver.findElement(By.xpath("//div[@class='bookingInfo__item'][6]/div[2]")).getText().split(" ")[0]);
	}
	
	public int getChildCount(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		return Integer.parseInt(driver.findElement(By.xpath("//div[@class='bookingInfo__item'][6]/div[2]")).getText().split(" ")[2]);
	}
	
	public String getRoomType(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		return driver.findElement(By.xpath("//div[@class='bookingInfo__item'][7]/div[2]")).getText();
	}
	
	public int getTotalCost(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		return Integer.parseInt(driver.findElement(By.xpath("//div[@class='bookingInfo__item'][8]/div[2]/span")).getText());
	}
	
	public boolean isPayAtHotelImageDisplayed(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		return driver.findElement(By.cssSelector("img[alt='Pay at hotel']")).isDisplayed();	
	}
	
	public void clickOnMakeAnotherBooking(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		driver.findElement(By.xpath("//a[text()='Make Another Booking']")).click();
	}
}
