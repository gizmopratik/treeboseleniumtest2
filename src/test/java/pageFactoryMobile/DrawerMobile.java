package pageFactoryMobile;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.AssertJUnit;

import io.selendroid.client.SelendroidDriver;
 


public class DrawerMobile {
 
    /**
     * All WebElements are identified by @FindBy annotation
     */
 
	SelendroidDriver driver;
 
    @FindBy(className="fa-bars")
    WebElement drawer;

    @FindBy(partialLinkText="Log In")
    WebElement login_link_drawer;
	
    public DrawerMobile(SelendroidDriver driver){
        this.driver = driver;
        //This initElements method will create all WebElements
        PageFactory.initElements(driver, this);
    }
 
    public void open_specific_link_in_drawer(String drawerLink) throws InterruptedException{
    	drawer.click();
    	Thread.sleep(3000);
    	
    	driver.findElement(By.partialLinkText(drawerLink)).click();
    	Thread.sleep(3000);    	
    }
 
}