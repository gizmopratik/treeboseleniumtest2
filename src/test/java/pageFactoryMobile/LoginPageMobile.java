package pageFactoryMobile;


import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.AssertJUnit;

import io.selendroid.client.SelendroidDriver;
 


public class LoginPageMobile {
 
    /**
     * All WebElements are identified by @FindBy annotation
     */
 
	SelendroidDriver driver;
 
    
    @FindBy(className="fa-bars")
    WebElement drawer;
    
    @FindBy(partialLinkText="Log In")
    WebElement login_link_drawer;
    
    
    @FindBy(id="id-username")
    WebElement emailId;
 
    @FindBy(id="id-password")
    WebElement password;

    
    @FindBy(className="btn-login-submit")
    WebElement loginButton;
 
    public LoginPageMobile(SelendroidDriver driver){
        this.driver = driver;
        //This initElements method will create all WebElements
        PageFactory.initElements(driver, this);
    }
 

    //Set user name in textbox
    public void setUserName(String strUserName) throws InterruptedException{
    	Thread.sleep(5000);
    	//emailId.click();
    	emailId.sendKeys(strUserName);
    }
 
    //Set password in password textbox
    public void setPassword(String strPassword){
    	password.sendKeys(strPassword);
    }
 
    //Click on login button
    public void clickLogin(){
    	loginButton.click();
    }
 
    /**
     * This POM method will be exposed in test case to login in the application
     * @param strUserName
     * @param strPasword
     * @return
     * @throws InterruptedException 
     */
    
    public void loginToTreeboMobileWeb(String strUserName,String strPasword) throws InterruptedException{
    	
    	drawer.click();
    	Thread.sleep(5000);
    	
    	login_link_drawer.click();
    	Thread.sleep(5000);
    	
    	enteruseridpassword(strUserName, strPasword);
    }
    
    
    public void enteruseridpassword(String strUserName, String strPasword) throws InterruptedException
    {
    	//Fill user name
        this.setUserName(strUserName);
        //Fill password
        this.setPassword(strPasword);
 
        //Click Login button
        this.clickLogin();
    	Thread.sleep(5000);
    }
    
 
}