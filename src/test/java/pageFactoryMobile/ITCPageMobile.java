package pageFactoryMobile;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.http.conn.ssl.BrowserCompatHostnameVerifier;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.touch.TouchActions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.AssertJUnit;

import io.selendroid.client.SelendroidDriver;

public class ITCPageMobile {

	/**
	 * All WebElements are identified by @FindBy annotation
	 */

	SelendroidDriver driver;

    @FindBy(className="fa-bars")
    WebElement drawer;
    

    @FindBy(className="detail")
    WebElement detail;

    @FindBy(className="cancel")
    WebElement cancel;
    
    @FindBy(id = "grandTotal")
	WebElement grandTotal;

	@FindBy(id = "payatHotel")
	WebElement payatHotel;

	@FindBy(name = "name")
	WebElement name;

	@FindBy(name = "email")
	WebElement email;

	@FindBy(name = "mobile")
	WebElement mobile;

	@FindBy(id = "logedinContinue")
	WebElement logedinContinue;

	@FindBy(id = "loginAndContinue")
	WebElement loginAndContinue;

	@FindBy(id = "continueGuest")
	WebElement continueGuest;

	@FindBy(className = "comments")
	WebElement comments;

	@FindBy(className = "copyright")
	WebElement copyright;

	@FindBy(id = "printReceipt")
	WebElement printReceipt;
	
	@FindBy(css = "#content > div > div:nth-child(2) > div.details-container.mb20 > div:nth-child(3) > div:nth-child(1) > a")
	WebElement cancelled_text;
	
	
	
	

	public void setName(String strName) {
		name.sendKeys(strName);
	}

	public void setEmail(String strEmail) {
		email.sendKeys(strEmail);
	}

	public void setMobile(String strMobile) {
		mobile.sendKeys(strMobile);
	}

	public ITCPageMobile(SelendroidDriver driver) {
		this.driver = driver;
		// This initElements method will create all WebElements
		PageFactory.initElements(driver, this);
	}

	public void click_on_continue_Btn() throws InterruptedException {
		System.out.println("Entering ITNERARY Page page");
		grandTotal.click();
		Thread.sleep(3000);

//		driver.manage().timeouts().pageLoadTimeout(40, TimeUnit.SECONDS);
		logedinContinue.click();
		Thread.sleep(30000);
		System.out.println("Leaving ITNERARY Page page");
	}

	public void click_on_payAtHotel_Btn() throws InterruptedException {
		System.out.println("Entering TRAVELLER PAGE Page page");
		Thread.sleep(5000);
		copyright.click();
		//driver.findElement(By.className("copyright")).click();

		Thread.sleep(5000);
		System.out.println("before clicking on Pay at hotel");
		
		driver.manage().timeouts().pageLoadTimeout(40, TimeUnit.SECONDS);
		
		System.out.println("CLICKING ON PAY AT HOTEL");
		payatHotel.click();

		System.out.println("After clicking on Pay at hotel");
		Thread.sleep(30000);
		
	//    wait.until(ExpectedConditions.elementToBeClickable(By.id("payatHotel")));
		AssertJUnit.assertEquals(true, printReceipt.isEnabled()); 
	//	AssertJUnit.assertEquals(driver.findElement(By.cssSelector("#content > div > div:nth-child(2) > div.details-container.mb20 > div:nth-child(3) > div:nth-child(1) > span")).getText(), "Cancelled");
		System.out.println("Leaving TRAVELLER Page page");	
		
	}

	public void enterGuestInformationOnConfirmationPage(String strName, String strEmail, String strMobile) {
		// Fill user name
		this.setName(strName);

		// Fill Mobile Number
		this.setEmail(strEmail);

		// Fill Email Id
		this.setMobile(strMobile);
	}

	public void cancelBookingMobile() throws InterruptedException {
		System.out.println("Enter BookingMobile");		
		DrawerMobile objDrawerMobile = new DrawerMobile(driver);

		driver.get("http://treebohotels.com");
		Thread.sleep(3000);
		
		objDrawerMobile.open_specific_link_in_drawer("Booking");
		
		detail.click();
		Thread.sleep(6000);
		System.out.println("Open first Detail row");
	
		cancel.click();
		Thread.sleep(5000);		
		
		driver.switchTo().alert().accept();
		Thread.sleep(15000);			
		
		System.out.println(driver.switchTo().alert().getText());
		AssertJUnit.assertEquals(driver.switchTo().alert().getText(), "Booking cancelled!");
		
		System.out.println("before OK");		
		driver.switchTo().alert().accept();
		System.out.println("after OK");
		Thread.sleep(4000);
		
		System.out.println("Before confimation of cancellaton popup");
		AssertJUnit.assertEquals(cancelled_text.getText(), "Cancelled");
		System.out.println("After confimation of cancellaton popup");
		
//		List<WebElement> links = driver.findElements(By.id("cancel_booking"));
//		AssertJUnit.assertEquals(links.get(0).getText(), "Cancelled");
	}

}