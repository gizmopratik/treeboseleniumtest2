//package TreeboUIMobileTest;
//
//import java.util.concurrent.TimeUnit;
//
////import io.selendroid.SelendroidCapabilities;
////import io.selendroid.SelendroidDriver;
//
//import org.openqa.selenium.By;
//import org.openqa.selenium.JavascriptExecutor;
//import org.openqa.selenium.WebDriver;
//import org.openqa.selenium.WebElement;
//import org.openqa.selenium.interactions.touch.TouchActions;
//import org.openqa.selenium.remote.DesiredCapabilities;
//import org.openqa.selenium.remote.RemoteWebDriver;
//import org.openqa.selenium.support.ui.ExpectedConditions;
//import org.openqa.selenium.support.ui.WebDriverWait;
//import org.testng.AssertJUnit;
//import org.testng.annotations.AfterSuite;
//import org.testng.annotations.BeforeSuite;
//import org.testng.annotations.Test;
//
//import PageFactory.HomePage;
//import PageFactory.ITCPage;
//import PageFactory.LoginPage;
//import io.selendroid.*;
//import io.selendroid.client.SelendroidDriver;
//import io.selendroid.common.SelendroidCapabilities;
//import io.selendroid.common.device.DeviceTargetPlatform;
//import io.selendroid.standalone.SelendroidConfiguration;
//import pageFactoryMobile.DrawerMobile;
//import pageFactoryMobile.ITCPageMobile;
//import pageFactoryMobile.LoginPageMobile;
//import pageFactoryMobile.SearchHDPageMobile;
//import pageFactoryMobile.SearchPopupMobile;
//import pageFactoryMobile.SearchResultPageMobile;
//
//public class BookHotelTestMobile {
//
//	SelendroidDriver driver = null;
//	// WebDriver driver = null;
//	LoginPageMobile objLogin;
//	SearchPopupMobile objSearchPopup;
//	SearchResultPageMobile objSearchResultPageMobile;
//	SearchHDPageMobile objSearchHDPageMobile;
//	ITCPageMobile objITCPageMobile;
//	DrawerMobile objDrawerMobile;
//	HomePage objHome;
//	ITCPage objITC;
//
//	@BeforeSuite
//	public void setUp() throws Exception {
//
//		// SelendroidConfiguration config = new SelendroidConfiguration();
//		// config.setSessionTimeoutSeconds(60000);
//
//		DesiredCapabilities caps = SelendroidCapabilities.android();
//		driver = new SelendroidDriver(caps);
//		// driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
//		// driver.manage().timeouts().pageLoadTimeout(100, TimeUnit.SECONDS);
//
//		driver.get("http://treebohotels.com");
//		// driver.get("http://www.treebohotels.com/hotels/treebo-hoppers-stop-hq0LrX35d/?checkIn=2015-11-24&checkOut=2015-11-25&guests=1&rooms=1");
//		Thread.sleep(2000);
//		
//		// WebDriver driver = new
//		// RemoteWebDriver(DesiredCapabilities.android());
//
//	}
//
//	@Test
//	public void BookHotelTestMobile() throws Exception {
//
//		// Create Login Page object
//		objLogin = new LoginPageMobile(driver);
//		objSearchPopup = new SearchPopupMobile(driver);
//		objSearchResultPageMobile = new SearchResultPageMobile(driver);
//		objSearchHDPageMobile = new SearchHDPageMobile(driver);
//		objITCPageMobile = new ITCPageMobile(driver);
//		objDrawerMobile = new DrawerMobile(driver);
//		objHome = new HomePage(driver);
//		objITC = new ITCPage(driver);
//
//		// login to application
//		objLogin.loginToTreeboMobileWeb("treebohotelsss@gmail.com", "ashish");
//
//		// Click on search textfield which is present on Home page
//		driver.findElement(By.id("searchBox")).click();
//
//		// Open Global search popup.
//		objSearchPopup.search_popup();
//
//		// Click on first result on search result page
//		objSearchResultPageMobile.click_on_first_result();
//
//		// Click on Book button - HD page		
//		objSearchHDPageMobile.click_on_book_button();
//
//		// Click on Continue button from Itnerary page
//		objITCPageMobile.click_on_continue_Btn();
//		
//		// Click on Pay at hotel - Traveller page
//		objITCPageMobile.click_on_payAtHotel_Btn();
//
//	}
//
//	@AfterSuite
//	public void tearDown() throws Exception {
//		 objITCPageMobile.cancelBookingMobile();
//		 driver.quit();
//	}
//
//}