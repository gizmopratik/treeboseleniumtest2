package PageFactory;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.support.ui.WebDriverWait;

import org.testng.Assert;

public class ITCPage {

    WebDriver driver;

	
    @FindBy(id="login-continue")
    WebElement continueITCButton;
	
    @FindBy(name="name")
    WebElement name;
 
    @FindBy(name="email")
    WebElement email;
	
    @FindBy(name="mobile")
    WebElement mobile;
	
    public void setName(String strName){
    	name.sendKeys(strName);
    }
    
    public void setEmail(String strEmail){
    	email.sendKeys(strEmail);
    }
    
    public void setMobile(String strMobile){
    	mobile.sendKeys(strMobile);
    }
    
    public ITCPage(WebDriver driver){
        this.driver = driver;
        //This initElements method will create all WebElements
        PageFactory.initElements(driver, this);
    }
    
    public void enterGuestInformationOnConfirmationPage(String strName, String strEmail, String strMobile){
    	//Fill user name
        this.setName(strName);
 
        //Fill Mobile Number
        this.setEmail(strEmail);
        
        //Fill Email Id
        this.setMobile(strMobile);
    }

	public void cancelBooking() throws InterruptedException{
		driver.get("http://treebohotels.com"); 
		Thread.sleep(3000);
		driver.findElement(By.className("dropdown-toggle")).click();
		Thread.sleep(7000);
		driver.findElement(By.linkText("Booking History")).click();	
		Thread.sleep(3000);
		driver.findElement(By.className("perNight")).click();
		Thread.sleep(3000);
		driver.findElement(By.className("cancle-booking")).click();	
		Thread.sleep(3000);
		driver.findElement(By.className("cancel-ok")).click();
		Thread.sleep(30000);	
		
		List<WebElement> links = driver.findElements(By.id("cancel_booking"));
		Assert.assertEquals(links.get(0).getText(), "Cancelled");
		}
}
