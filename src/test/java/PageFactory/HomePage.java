package PageFactory;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class HomePage {

    /**
     * All WebElements are identified by @FindBy annotation
     */
 
    WebDriver driver;
 
    @FindBy(className="search-btn")
    WebElement searchbtn;
 
 
    public HomePage(WebDriver driver){
        this.driver = driver;
        //This initElements method will create all WebElements
        PageFactory.initElements(driver, this);
    }
    
    //Click on Search Button from Home Page
    public void clickSearchButton(){
    	searchbtn.click();
    }
	
    public boolean checksearchbutton(){
    	return driver.findElements(By.className("search-btn")).size() != 0;
    }
    
    
    
}
