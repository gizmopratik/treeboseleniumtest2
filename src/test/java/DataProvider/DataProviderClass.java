package DataProvider;

import org.testng.annotations.DataProvider;

public class DataProviderClass {
    @DataProvider(name = "data-provider")
    public static Object[][] dataProviderMethod()
    {
        return new Object[][] { { "data one" }, { "data two" } };
    }
    
    
    @DataProvider(name = "guest_page_url")
    public static Object [][] guest_page_url(){
    	return new Object[][] {{ "http://treebohotels.com" }};
    }
 }
