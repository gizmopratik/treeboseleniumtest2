package desktopUIPageObjects;

import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import desktopUIPageObjects.HomePage;

import base.BrowserHelper;
import base.DriverManager;

public class Account {
	private By accountName = By.xpath("//li[@id='user']/div/button");
	private By myAccount = By.xpath("//li[@id='user']//a[@href='/account/']");
	private By settingsLink = By.cssSelector(".settings");
	private By profileLink = By.cssSelector(".profile");
	private By oldPassword = By.cssSelector("#oldPwd");
	private By newPassword = By.cssSelector("#newPwd");
	private By confirmPassword = By.cssSelector("#confirmPwd");
	private By savePassword = By.cssSelector("#submitPasswordChange");
	private By signoutLink = By.xpath("//li[@id='user']//a[@href='/logout/']");
	private By signedInUser = By.cssSelector(".user__name.dropbtn");
	
	private By profileDataStored = By.cssSelector("#profileDetailData");
	private By firstNameLocator = By.cssSelector("#firstName");
	private By lastNameLocator = By.cssSelector("#lastName");
	private By mobileLocator = By.cssSelector("#mobile");
	private By emailLocator = By.xpath("//input[@id='email'][@readonly]");
	private By saveProfileButton = By.cssSelector("#submitProfileDetail");
	
	public void goToAccountPage() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		WebDriverWait wait = new WebDriverWait(driver, 120);
		wait.until(ExpectedConditions.presenceOfElementLocated(accountName));
		driver.findElement(accountName).click();
		driver.findElement(myAccount).click();
	}
	
	public void goToSettingsPage() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		WebDriverWait wait = new WebDriverWait(driver, 120);
		wait.until(ExpectedConditions.presenceOfElementLocated(accountName));
		driver.findElement(accountName).click();
		driver.findElement(myAccount).click();
		driver.findElement(settingsLink).click();
	}
	
	public void goToProfilePage() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		WebDriverWait wait = new WebDriverWait(driver, 120);
		wait.until(ExpectedConditions.presenceOfElementLocated(accountName));
		driver.findElement(accountName).click();
		driver.findElement(myAccount).click();
		driver.findElement(profileLink).click();
	}
	
	public void changePassword(String strOldPassword,String strNewPassword){
		WebDriver driver = DriverManager.getInstance().getDriver();
		driver.findElement(oldPassword).sendKeys(strOldPassword);
		driver.findElement(newPassword).sendKeys(strNewPassword);
		driver.findElement(confirmPassword).sendKeys(strNewPassword);
		driver.findElement(savePassword).click();
	}
	
	public void doSignOut() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		System.out.println("Click on Signout ");
		driver.findElement(signedInUser).click();
		driver.findElement(signoutLink).click();
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void updateNameAndMobile(String strFirstName, String strLastName, String strMobile){
		WebDriver driver = DriverManager.getInstance().getDriver();
		driver.findElement(firstNameLocator).clear();
		driver.findElement(firstNameLocator).sendKeys(strFirstName);;
		driver.findElement(lastNameLocator).clear();
		driver.findElement(lastNameLocator).sendKeys(strLastName);;
		driver.findElement(mobileLocator).clear();
		driver.findElement(mobileLocator).sendKeys(strMobile);;

		// verify that email is read only and cannot be edited/updated
		Assert.assertTrue(driver.findElement(emailLocator).isDisplayed());
		driver.findElement(saveProfileButton).click();
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	// Keys are
	// [work_email, gender, phone, city, dob, last_name, first_name, anniversary, email]
	public String getProfileFieldValue(String key){
		Map<String,String> mapProfileData = returnProfileData();
		String profileFieldValue = mapProfileData.get(key);
		System.out.println("Value of profile field " + key + " is : " + profileFieldValue);
		return profileFieldValue;
	}
	
	public Map<String,String> returnProfileData(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		String profielData = driver.findElement(profileDataStored).getAttribute("value");
		return convert(profielData);
	}
	
	// {"phone": "2000000000", "first_name": "Test", "last_name": "Test", "city": "India", "dob": "1989-01-05", 
	// "gender": "M", "work_email": "treebotestaccount@gmail.com", "anniversary": "2001-01-31", 
	 // "email": "treebotestaccount@gmail.com"} 
	
	public static Map<String, String> convert(String str) {
	    String[] tokens = str.replaceAll("\\{|\\}|\"|\"", "").split(":|,");
	    Map<String, String> map = new HashMap<String, String>();
	    for (int i=0; i<tokens.length-1; ) map.put(tokens[i++].trim(), tokens[i++].trim());
	    return map;
	}
}
