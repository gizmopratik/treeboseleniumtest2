import datetime
import hmac
from hashlib import sha1
import requests
from pyquery import PyQuery as pQ
import sys

time = datetime.datetime.utcnow().isoformat()
#print time
xml = """<?xml version='1.0'?>
			<hotelogix version='1.0' datetime='%(time)s'>
			   <request method='wsauth' key='%(key)s'>
				</request>
			</hotelogix>
			""" % {'time': time, 'key': 'B39CEE50AC4B6925F9697409E2D84C88746593E5'}

#print xml

signature1 = hmac.new('9110CC27311F94542E7F5ACF3643B3909029F570', xml, sha1)
signature = signature1.hexdigest()
#print signature

headers = {'Content-Type': 'text/xml', 'X-HAPI-Signature': signature}
response = requests.post("https://crs.staygrid.com/ws/web/" + "wsauth", data=xml, headers=headers)
#print response.content

d = pQ(response.content)
access_key = d("accesskey").attr("value")
access_secret = d("accesssecret").attr("value")

#print access_key
#print access_secret

#checkindate = "2016-04-26"
#checkoutdate = "2016-04-27"
checkindate = sys.argv[1]
checkoutdate = sys.argv[2]
adultCount = 1
childCount = 0
infantCount = 0
roomRequired = 0
limit = 0
offset = 0
hasResult = 0

searchxml = """<?xml version='1.0'?>
    <hotelogix version='1.0' datetime='%(time)s'>
    <request method='search' key='%(key)s'>
    <stay checkindate='%(checkin_date)s' checkoutdate='%(checkout_date)s'/>
    <pax adult='%(adult_count)s' child='%(child_count)s' infant='%(infant_count)s'/>
    <roomrequire value='%(room_required)s'/>
    <ignorelists>
    <bookingpolicy/>
    <cancellationpolicy/>
    <roomtypeimage/>
    <amenity/>
    <rateimage/>
    <inclusion/>
    </ignorelists>
    <limit value='%(limit)s' offset='%(offset)s' hasResult='%(has_result)s'/>
    </request>
    </hotelogix>
    """ % {'time': time, 'checkin_date': checkindate,
        'checkout_date': checkoutdate, 'adult_count': str(adultCount), 'child_count': str(childCount),
            'infant_count': str(infantCount), 'room_required': str(roomRequired), 'limit': str(limit),
                'offset': str(offset), 'has_result': str(hasResult), 'key' : access_key}

signatureSearch = hmac.new(str(access_secret), str(searchxml), sha1).hexdigest()
#print signatureSearch
headersSearch = {'Content-Type': 'text/xml', 'X-HAPI-Signature': signatureSearch}
responseSearch = requests.post("https://crs.staygrid.com/ws/web/" + "search", data=searchxml, headers=headersSearch)
print responseSearch.content
dS = pQ(responseSearch.content)
hotelName = dS("hotel").attr("title")
#print hotelName


